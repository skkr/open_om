% cost input
cost_filename='Inputs_costs.xlsx';

%direct costs
materials_cost= xlsread(cost_filename, 'Material costs WT');
vessel_dayrates=xlsread(cost_filename, 'Vessel dayrates');
mobilisation_cost=xlsread(cost_filename, 'Mobilisation cost');
demobilisation_cost=xlsread(cost_filename, 'Demobilisation cost');
if isempty(demobilisation_cost)
    demobilisation_cost=0;
end
    
crew_rates=xlsread(cost_filename, 'Crew dayrates');
energy_price=xlsread(cost_filename, 'Energy price');
materials_cost_planned=xlsread(cost_filename, 'Planned maintenance');

%indirect costs
energy_price=xlsread(cost_filename, 'Energy price'); 

%interest rate
ir=xlsread(cost_filename, 'Interest rate'); 

