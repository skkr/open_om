function [F] = cumulative(failurerate,MTTF)
%it calculates the probability of the  selected failure 
F=1-exp(-failurerate*MTTF);
%JCHR: the occurrence of a failure event is a Poisson arrival process, with
%distribution:
F2 = failurerate*MTTF*exp(-failurerate*MTTF);
end

