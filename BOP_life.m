function [time_abs_BOP,tot_downtime_BOP] = BOP_life(w_forecast)


% it calculates MTTF through the exponential CDF

Read_Input_WF;

%%JW: Where is this comment coming from? Do we need it?
% in the future kd will be used to simulate the failure for the entire
% turbine as it will consider all the subsystem


%calculates lambda ratios
% [l_total,l_turbine,l_mih,l_mah,l_replh,l_hour,l_turb_hour]=lamba_ratios(lambda);
%num_j defines among how many iteration the user wants to find the final
%MTTF
num_j=10;
%num_i defines how many random failures are simuated per iteration
num_i=1000;
%preallocating arrays for computational time savings
F=ones([num_i,1]);
MTTF1_tot=ones([num_i,1]);
CDF=ones([num_i,num_j]);
MTTF_ord=ones([num_i,num_j]);
MTTF_tot=ones([1,num_j]);
%

max_vessel_avlb=nr_vessel_UP;
CREW_req_avlb=nr_crew_UP;


[r_BOP,c_BOP]=size(failure_rate_BOP);
MTTF_subsystem_BOP=ones([r_BOP,n_BOP]);
MTTF_subs_modes_BOP=ones([r_BOP,c_BOP,n_BOP]);
time_BOP=zeros([n_BOP,1]);
tot_downtime_spare_BOP=zeros([n_BOP,1]);
tot_downtime_org_BOP=zeros([n_BOP,1]);
tot_downtime_repair_BOP=zeros([n_BOP,1]);
tot_downtime_sea_BOP=zeros([n_BOP,1]);
tot_downtime_wind_BOP=zeros([n_BOP,1]);
tot_downtime_vessel_BOP=zeros([n_BOP,1]);
tot_downtime_crew_BOP=zeros([n_BOP,1]);
downtime_vessel_BOP=zeros([n_BOP,1]);
downtime_crew_BOP=zeros([n_BOP,1]);
downtime_sea_BOP=zeros([n_BOP,1]);
downtime_wind_BOP=zeros([n_BOP,1]);
downtime_org_BOP=zeros([n_BOP,1]);
downtime_spare_BOP=zeros([n_BOP,1]);

[l_total_BOP,l_turbine_BOP,l_mih_BOP,l_hour_BOP,l_turb_hour_BOP]=lambda_ratios_BOP(failure_rate_BOP);
it=1;
tot_downtime_BOP=0; %JW: needs to be initialised for the case that no downtime is caused by BOPs
test_BOP=1;
b_BOP=(time_BOP>lifetime_hours);
while test_BOP==1
    if sum(b_BOP)==n_BOP
        test_BOP=0;
    else
        for td=1:n_BOP

            %hours
            MTTF_aver_hours_tot_BOP=1/l_turb_hour_BOP;
            %days
            MTTF_aver_final_days_tot_BOP=MTTF_aver_hours_tot_BOP/24;
            %years
            MTTF_years_tot_BOP=MTTF_aver_final_days_tot_BOP/365;
            

            MTTF_subsystem_BOP(:,td)=MTTF_subsystem_BOP(:,td)+MTTF_aver_hours_tot_BOP;
                
            MTTF_subs_modes_BOP(:,:,td)=MTTF_subs_modes_BOP(:,:,td)+MTTF_aver_hours_tot_BOP;

            
            %probability to subsystem x to fail
            smp_fail_times = exprnd(1./l_hour_BOP);
            mode_subsystem_BOP = find (smp_fail_times == min(smp_fail_times));
            
            
            % %hereafter the failure mode is randomly selected taking into account
%             %probabilities as weights
            smp_fail_times_modes = exprnd(1./l_mih_BOP(mode_subsystem_BOP,:));
            mode_BOP = find (smp_fail_times_modes == min(smp_fail_times_modes)); % 1=minor, 2=major, 3=replacement
            
            %creating matrices for all the turbines
            
            MTTF_BOP(1,td)=MTTF_aver_hours_tot_BOP;
            subs_failure_BOP(1,td)=mode_subsystem_BOP;
            mode_subs_failure_BOP(1,td)=mode_BOP;
            
            time_BOP(td,1)=time_BOP(td,1)+MTTF_BOP(1,td);
            if  time_BOP(td,1)>lifetime_hours
                time_BOP(td,1)=lifetime_hours+1;
            end
            time_abs_BOP(td,it)= time_BOP(td,1);
            %     count_failures_BOP(mode_subsystem_BOP,mode_BOP)=count_failures_BOP(mode_subsystem_BOP,mode_BOP)+1;
        end
        %sorting matrices
        
        [time_sort_BOP,p_BOP]=sort(time_BOP);
        subs_sort_BOP=subs_failure_BOP(p_BOP);
        mode_sort_BOP=mode_subs_failure_BOP(p_BOP);
        %depending on the result above the output are calculated
        ini_BOP=1;
        iniz_BOP=1;
        first_BOP=1;
        for kd=1:n_BOP
            if time_BOP(p_BOP(kd),1)>lifetime_hours
                continue
            end
            %     time_turbines(p_turb(kd),1)=time_turbines(p_turb(kd),1);
            ves_req_BOP(kd,1)=required_main_vessel_BOP(subs_sort_BOP(kd),mode_sort_BOP(kd));
            
            CREW_req_BOP(kd,1)=required_crew_BOP(subs_sort_BOP(kd),mode_sort_BOP(kd));
           % nbr_shifts_required(kd,1)=repair_time(subs_sort(kd),mode_sort(kd))/(work_shift-2*tr_time(ves_req(kd,1)));
            
            if kd>=2
                for fd=first_BOP:kd-1
                    %                 if ves_req(kd,1)-ves_req(fd,1)==0
                    if time_end_mission_BOP(fd,1)-time_sort_BOP(kd,1)<=0 % nr_vessel_UP(ves_req(fd,1))-max_vessel_avlb(ves_req(fd,1))<0
                        if nr_vessel_UP(ves_req_BOP(fd,1))-max_vessel_avlb(ves_req_BOP(fd,1))<0  && nr_crew_UP-CREW_req_avlb<0
                            nr_vessel_UP(ves_req_BOP(fd,1))=nr_vessel_UP(ves_req_BOP(fd,1))+1;
                            nr_crew_UP=nr_crew_UP+CREW_req_BOP(fd,1);
                            first_BOP=fd;
                        end
                    end
                    %                 elseif ves_req-ves_req_prev(kd-1)~=0
                    %                     nr_vessel_UP(ves_req_prev)=nr_vessel_UP(ves_req_prev)+1;
                    %                     nr_crew_UP=nr_crew_UP+CREW_req;
                end
            end
            %             nr_vessel_UP(ves_req(kd,1))=nr_vessel_UP(ves_req(kd,1))-1;
            %             nr_crew_UP=nr_crew_UP-CREW_req(kd,1);
            
            if kd>=2
                if nr_vessel_UP(ves_req_BOP(kd,1))>0
                    nr_vessel_UP(ves_req_BOP(kd,1))=nr_vessel_UP(ves_req_BOP(kd,1))-1;
                else
                    for fd=ini_BOP:kd-1
                        if nr_vessel_UP(ves_req_BOP(fd,1))-max_vessel_avlb(ves_req_BOP(fd,1))<0  && nr_crew_UP-CREW_req_avlb<0
                            if time_end_mission_BOP(fd,1)-time_sort_BOP(kd,1)>0 && ves_req_BOP(kd,1)-ves_req_BOP(fd,1)==0
                                nr_vessel_UP(ves_req_BOP(fd,1))=nr_vessel_UP(ves_req_BOP(fd,1))+1;
                                nr_crew_UP=nr_crew_UP+CREW_req_BOP(fd,1);
                                time_BOP(p_BOP(kd),1)=time_BOP(p_BOP(kd),1)+abs(time_end_mission_BOP(fd,1)-time_sort_BOP(kd,1));
                                %                     if time_turbines(p_turb(kd),1)>lifetime_hours
                                %                         continue
                                %                     end
                                 if ne(mode_subs_failure_BOP(1,p_BOP(kd)),1)
                                tot_downtime_vessel_BOP(p_BOP(kd),1)=tot_downtime_vessel_BOP(p_BOP(kd),1)+abs(time_end_mission_BOP(fd,1)-time_sort_BOP(kd,1));
                                downtime_vessel_BOP(p_BOP(kd),1)=abs(time_end_mission_BOP(fd,1)-time_sort_BOP(kd,1));
                                 else
                                     downtime_vessel_BOP(p_BOP(kd),1)=0;
                                 end
                                ini_BOP=fd;
                                continue
                            end
                        end
                    end
                end
            end
            
            if kd>=2
                
                if nr_crew_UP>=CREW_req_BOP(kd,1)
                    nr_crew_UP=nr_crew_UP-CREW_req_BOP(kd,1);
                else
                    crew_missing_BOP=abs(nr_crew_UP-CREW_req_BOP(kd,1));
                    
                    for fd=ini_BOP:kd-1
                        if nr_vessel_UP(ves_req_BOP(fd,1))-max_vessel_avlb(ves_req_BOP(fd,1))<0  && nr_crew_UP-CREW_req_avlb<0
                            if time_end_mission_BOP(fd,1)-time_sort_BOP(kd,1)>0
                                nr_crew_UP=nr_crew_UP+CREW_req_BOP(fd,1);
                                nr_vessel_UP(ves_req_BOP(fd,1))=nr_vessel_UP(ves_req_BOP(fd,1))+1;
                                if nr_crew_UP-crew_missing_BOP>0
                                    time_BOP(p_BOP(kd),1)=time_BOP(p_BOP(kd),1)+abs(time_end_mission_BOP(fd,1)-time_sort_BOP(kd,1));
                                    %                     if time_turbines(p_turb(kd),1)>lifetime_hours
                                    %                         continue
                                    %                     end
                                     if ne(mode_subs_failure_BOP(1,p_BOP(kd)),1)
                                    tot_downtime_crew_BOP(p_BOP(kd),1)=tot_downtime_crew_BOP(p_BOP(kd),1)+abs(time_end_mission_BOP(fd,1)-time_sort_BOP(kd,1));
                                    downtime_crew_BOP(p_BOP(kd),1)=abs(time_end_mission_BOP(fd,1)-time_sort_BOP(kd,1));
                                     else
                                         downtime_crew_BOP(p_BOP(kd),1)=0; 
                                     end
                                    ini_BOP=fd;
                                    continue
                                end
                            end
                        end
                    end
                end
            end
            
            if spare_stock_BOP(subs_sort_BOP(kd),mode_sort_BOP(kd))==0
                time_BOP(p_BOP(kd),1)=time_BOP(p_BOP(kd),1)+spare_wait_BOP(subs_sort_BOP(kd),mode_sort_BOP(kd));
                %                 if time_turbines(p_turb(kd),1)>lifetime_hours
                %                     time_turbines(p_turb(kd),1)=time_turbines(p_turb(kd),1)-SWT(subs_sort(kd),mode_sort(kd));
                %                     continue
                %                 end
                  if ne(mode_subs_failure_BOP(1,p_BOP(kd)),1)
                tot_downtime_spare_BOP(p_BOP(kd),1)=tot_downtime_spare_BOP(p_BOP(kd),1)+spare_wait_BOP(subs_sort_BOP(kd),mode_sort_BOP(kd));
                downtime_spare_BOP(p_BOP(kd),1)=spare_wait_BOP(subs_sort_BOP(kd),mode_sort_BOP(kd));
                  else
                downtime_spare_BOP(p_BOP(kd),1)=0;
                  end
            end
            time_BOP(p_BOP(kd),1)=time_BOP(p_BOP(kd),1)+org_time_BOP(mode_subsystem_BOP,mode_BOP)+mob_time(ves_req_BOP(kd,1));
            if time_BOP(p_BOP(kd),1)>lifetime_hours
%                 time_BOP(p_BOP(kd),1)=time_BOP(p_BOP(kd),1)-org_time_BOP(mode_subsystem_BOP,mode_BOP)-mob_time(ves_req_BOP(kd,1));
                continue
            end
             if ne(mode_subs_failure_BOP(1,p_BOP(kd)),1)
            tot_downtime_org_BOP(p_BOP(kd),1)=tot_downtime_org_BOP(p_BOP(kd),1)+org_time_BOP(mode_subsystem_BOP,mode_BOP)+mob_time(ves_req_BOP(kd,1));
            downtime_org_BOP(p_BOP(kd),1)=org_time_BOP(mode_subsystem_BOP,mode_BOP)+mob_time(ves_req_BOP(kd,1));
             else
                 downtime_org_BOP(p_BOP(kd),1)=0;
             end
             

            navigation_time_BOP=ceil(repair_time_BOP(subs_sort_BOP(kd),mode_sort_BOP(kd))/(work_shift-2*tr_time(ves_req_BOP(kd,1))))*2*tr_time(ves_req_BOP(kd,1))+repair_time_BOP(subs_sort_BOP(kd),mode_sort_BOP(kd));
            starting_seastate_BOP=floor(time_BOP(p_BOP(kd),1)/time_step);
            ending_seastate_BOP=ceil((time_BOP(p_BOP(kd),1)+navigation_time_BOP)/time_step);
            number_seastate_BOP=ending_seastate_BOP-starting_seastate_BOP+1;
            max_wv_BOP=max_wave(ves_req_BOP(kd,1));
            max_wd_BOP=max_wind(ves_req_BOP(kd,1));
            b_wave_BOP=(w_forecast(:,column_wave)<=max_wv_BOP);
            b_wind_BOP=(w_forecast(:,column_wind)<=max_wd_BOP);
            test=1;
            [r1,~]=size(w_forecast);
            itp=0;
            while test==1
                helper_wave_BOP=b_wave_BOP(starting_seastate_BOP:ending_seastate_BOP)-ones([ending_seastate_BOP-starting_seastate_BOP+1,1]);
                helper_wind_BOP=b_wind_BOP(starting_seastate_BOP:ending_seastate_BOP)-ones([ending_seastate_BOP-starting_seastate_BOP+1,1]);
                
                if ending_seastate_BOP>r1
                    disp('mare non sicuro')
                    break
                end
                
                if sum(helper_wave_BOP)==0 && sum(helper_wind_BOP)==0
                    test=0;
                elseif sum(helper_wave_BOP)~=0 && sum(helper_wind_BOP)==0 || sum(helper_wave_BOP)~=0 && sum(helper_wind_BOP)~=0
                    for id=starting_seastate_BOP:ending_seastate_BOP
                        if w_forecast(id,column_wave)-max_wv_BOP>0
                            time_BOP(p_BOP(kd),1)=time_BOP(p_BOP(kd),1)+(id+1-starting_seastate_BOP)*time_step;
                            %                             if time_turbines(p_turb(kd),1)>lifetime_hours
                            %                                 time_turbines(p_turb(kd),1)=time_turbines(p_turb(kd),1)-(id+1-starting_seastate)*time_step;
                            %                                 continue
                            %                             end
%                               if ne(mode_subs_failure_BOP(1,p_BOP(kd)),1)
                            tot_downtime_sea_BOP(p_BOP(kd),1)=tot_downtime_sea_BOP(p_BOP(kd),1)+(id+1-starting_seastate_BOP)*time_step;
                            downtime_sea_BOP(p_BOP(kd),1)=(id+1-starting_seastate_BOP)*time_step;
%                               end
                            ending_seastate_BOP=ending_seastate_BOP+id+1-starting_seastate_BOP;
                            starting_seastate_BOP=starting_seastate_BOP+id+1-starting_seastate_BOP;
                            break
                        end
                    end
                elseif sum(helper_wave_BOP)==0 && sum(helper_wind_BOP)~=0
                    for id=starting_seastate_BOP:ending_seastate_BOP
                        if w_forecast(id,column_wind)-max_wd_BOP>0
                            time_BOP(p_BOP(kd),1)=time_BOP(p_BOP(kd),1)+(id+1-starting_seastate_BOP)*time_step;
%                              if ne(mode_subs_failure_BOP(1,p_BOP(kd)),1)
                            tot_downtime_wind_BOP(p_BOP(kd),1)=tot_downtime_wind_BOP(p_BOP(kd),1)+(id+1-starting_seastate_BOP)*time_step;
                            downtime_wind_BOP(p_BOP(kd),1)=(id+1-starting_seastate_BOP)*time_step;
%                              end
                            ending_seastate_BOP=ending_seastate_BOP+id+1-starting_seastate_BOP;
                            starting_seastate_BOP=starting_seastate_BOP+id+1-starting_seastate_BOP;
                            break
                        end
                    end
                end
                itp=itp+1;
            end
            
            time_BOP(p_BOP(kd),1)=time_BOP(p_BOP(kd),1)+tr_time(ves_req_BOP(kd,1));
            if time_BOP(p_BOP(kd),1)>lifetime_hours
                time_BOP(p_BOP(kd),1)=time_BOP(p_BOP(kd),1)-tr_time(ves_req_BOP(kd,1));
                continue
            end
            
            
            tot_downtime_repair_BOP(p_BOP(kd),1)=tot_downtime_repair_BOP(p_BOP(kd),1)+repair_time_BOP(subs_sort_BOP(kd),mode_sort_BOP(kd));
            downtime_repair_BOP(p_BOP(kd),1)=repair_time_BOP(subs_sort_BOP(kd),mode_sort_BOP(kd));
            %         MTTF_array(iteration,:)=([iteration,MTTF_aver_hours_tot,time_turbines,tot_downtime_repair,mode,mode_subsystem,starting_seastate,ending_seastate]);
            MTTF_subsystem_BOP(subs_sort_BOP(kd),p_BOP(kd))=0;
            MTTF_subs_modes_BOP(subs_sort_BOP(kd),mode_sort_BOP(kd),p_BOP(kd))=0;
            time_BOP(p_BOP(kd),1)=time_BOP(p_BOP(kd),1)+repair_time_BOP(subs_sort_BOP(kd),mode_sort_BOP(kd));
            if time_BOP(p_BOP(kd),1)>lifetime_hours
                time_BOP(p_BOP(kd),1)=time_BOP(p_BOP(kd),1)-repair_time(subs_sort_BOP(kd),mode_sort_BOP(kd));
                continue
            end
            %             if mode_sort_BOP(kd)==3
            %                 MTTF_subs_modes_BOP(subs_sort_BOP(kd),:,p_BOP(kd))=0;
            %             end
            time_end_mission_BOP(kd,1)=time_BOP(p_BOP(kd),1)+tr_time(ves_req_BOP(kd,1))+demob_time(ves_req_BOP(kd,1));
            
          tot_downtime_BOP(kd,it)=downtime_vessel_BOP(kd,1)+downtime_crew_BOP(kd,1)+downtime_org_BOP(kd,1)+downtime_spare_BOP(kd,1)+downtime_sea_BOP(kd,1)+downtime_wind_BOP(kd,1)+downtime_repair_BOP(kd,1);
  
        end
        b_BOP=(time_BOP(:,1)>lifetime_hours);
        
    end
    it=it+1;
end
downtime_wind_farm_BOP=tot_downtime_BOP*n_turbines;
tot_downtime_BOP;
time_abs_BOP;
end

