function [Prob, total_i] = Prob_matrix2(month, History_discrete, column_month, column_wave, wave_values, n_wave)
% Function to determine the probability matrix for the specified month

total_i = zeros(n_wave,1); % Variable where the total number of ocurrences of each wave height is stored (for this month)
Prob = zeros(n_wave); % Probability matrix for this month

for i=1:size(History_discrete,1)-1 % Goes through all the rows of the discretised historic matrix
    
    if(History_discrete(i,column_month)==month) % Checks if the month is the specified one
        
        for j=1:n_wave % This loop checks what of the discrete values of wave height is "taking place"
            
            if(History_discrete(i,column_wave)==wave_values(j)) % When the specific value is found, its total number of ocurrences increases by one and the state to which it goes is checked
                
                total_i(j,1) = total_i(j,1) + 1;
                
                for k=1:n_wave % This loop checks the next wave height value
                    if(History_discrete(i+1,column_wave)==wave_values(k))
                        Prob(j,k) = Prob(j,k) + 1; % When that value is found, the probability of that specific transition is increased
                        break; % Once we know the transition, we can exit the loop with this line
                    end
                end
                
                break; % Once we find the present value and its transition we can exit the loop
                
            end
            
        end
        
    end
    
end


for i=1:n_wave % Loop to determine the actual probabilities, by dividing transition counts by total amount of state instances
    if(total_i(i,1)>0)
        Prob(i,:) = 1/total_i(i,1) * Prob(i,:);
    else
        Prob(i,:) = 2; % When a wave height value has not appeared (its total count is zero), the previous division would be impossible. A probability of 2 is imposed for all its transitions, which means that said state (wave height) does not happen in this month
    end
end

end

