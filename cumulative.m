function [F] = cumulative(failurerate,MTTF)
%it calculates the probability of the  selected failure 
F=1-exp(-failurerate.*MTTF);
end

