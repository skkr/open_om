% function [l_total,l_turbine,l_modes,l_hour,l_turb_hour] = lamba_ratios(lambda)
% %It calculates all lambda ratios
% [r,c]=size(lambda);
% l_total=ones([r,1]);
% l_modes=ones([r,c]);
% l_hour=ones([r,1]);
% for id=1:r %loop for every subsystem
%     l_total(id,:)=sum(lambda(id,:));
%     l_turbine=sum(l_total);
%     
%     for jd=1:c %loop for every failure modes (1=minor, 2=major, 3=replacement)
%         l_modes(id,jd)=lambda(id,jd)./(365*24); 
%     end
%     
%     l_hour(id,:)=l_total(id,:)./(365*24);
%     l_turb_hour=l_turbine./(365*24);
% end


%% JCHR: new version of lamba_ratios function using vectorisation

function [l_hour, l_modes, l_turb_hour] = lamba_ratios(lambda_turbine, td)
%It calculates all lambda ratios

l_modes = lambda_turbine(:,:,td)/(365*24);
l_total = sum(lambda_turbine(:,:,td),2);
l_hour = l_total./(365*24);

l_turbine = sum(l_total); % so we are assuming a serialised system
l_turb_hour=l_turbine./(365*24);



