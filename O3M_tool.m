clc
clear variables
clf

Read_Input_WF;
Read_Input_Costs;
[lambda, r, c]=Read_Input_Turbines(1);

lambda_turbine=zeros(r, c, n_turbines);

for td=1:n_turbines
    [lambda, r, c]=Read_Input_Turbines(td);
    lambda_turbine(:,:,td)=lambda;
end

avrg=10;
% the following arrays preallocates the averaged data in order to save time
sum_downtime_fianle=zeros([avrg,14]);
DOWNTIME_TOTAL_unplanned=zeros([avrg,1]);
DOWNTIME_TOTAL_BOP=zeros([avrg,1]);
DOWNTIME_TOTAL=zeros([avrg,1]);
tot_planned_maintenance=zeros([avrg,1]);
availability=zeros([avrg,2]);
power_tot=zeros([n_turbines,avrg]);
power_tot_finale=zeros([n_turbines,1]);
mission_num=0;
power_lost_tot=zeros([n_turbines,avrg]);
power_lost_planned=zeros([n_turbines,avrg]);


%initialise excel file
warning('off', 'MATLAB:xlswrite:AddSheet');
xlswrite('Output_costs.xlsx',{'Iteration','Availability (%)','Energy (GWh)','Revenue (�)',},'Final Availability');
xlswrite('Output_costs.xlsx',{'Iteration','Materials Unplanned(�)','Vessels Unplanned (�)','Mobilisation Unplanned (�)','Demobilisation Unplanned (�)','Crew Unplanned (�)',...
    'Materials Planned (�)','Vessels Planned (�)','Crew Planned (�)'},'Direct Costs')
xlswrite('Output_costs.xlsx',{'Iteration','Lost Production (�)','Lost Production Planned (�)'},'Indirect Costs');
outputtable_JUV=table();
outputtable_CTV=table();
npvtable=cell(1,3); %columns for each iteration: year, direct costs, indirect costs, revenue
%eventtable  = {'Iteration','Event','Year','Turbine','Vessel_Used','Number_of_Vessels','Number_of_Crew'};



% THE SIMULATION STARTS
 % number of iteration that allows to average the results (JCHR: Why 20? This could be determined based on a convergence criterium)
for itd=1:avrg %JCHR: These repetitions could be avoided. We could just rewrite the tool for one repetition, and then call the tool several times if we want to make an average. This can (must) be run in parallel
    
    
    
    %SIMULATING SEA AND WIND CONDITION IN ALL THE WIND FARM LIFE
    w_forecast=weather_forecast3(wheather_yrs,time_step);
    disp(['Iteration number ', num2str(itd),' started.'])

    
    %num_j defines among how many iteration the user wants to find the final
    %MTTF
    num_j=3;
    %num_i defines how many random failures are simuLated per iteration
    num_i=1000;
    
    %preallocating arrays for computational time savings
    F=ones([num_i,1]);
    MTTF1_tot=ones([num_i,1]);
    CDF=ones([num_i,num_j]);
    MTTF_ord=ones([num_i,num_j]);
    MTTF_tot=ones([1,num_j]);
    
    %cost arrays
    vessels_used=zeros(1,vessel_types);
    crew_used=0;
    campaign_used=zeros(1,vessel_types);
    materials_used=zeros(7,3);
   

    
    iteration=1; %JW: counts how many iterations needed to reach for all turbines end of lifetime of WF
    
    Read_Input_WF;

    tot_downtime=0;
    MTTF_hours_tot_sum_minor=zeros([r,1]);
    MTTF_hours_tot_sum_major=zeros([r,1]);
    MTTF_hours_tot_sum_repl=zeros([r,1]);
    MTTF_aver_hours_tot=zeros([r,1]);
    MTTF_years_tot=zeros([r,1]);
    count_failures=zeros([r,c]);
    MTTF_subs_modes=ones([r,c,n_turbines]);
    P_failure_minor=zeros([c,1]);
    P_array=zeros([1,c]);
    residui=1000000000*ones([r,1]);
    MTTF_subsystem=ones([r,n_turbines]);
    P_failure_subsystem=ones([r,n_turbines]);
    time_end_mission=zeros([n_turbines,1]);
    vessel_crew_list=NaN([n_turbines, 4]);
    JUV_list=[];
    CTV_list=[];
    CTV_mission_list=[];
    time_end_mission_prev=0;
    mission_ves_req=[];
    mission_ves_req_details=[];
    min_time_end_mission=[];
    index=[];
    prev_campaign_crew=0;
    time_end_campaign=lifetime_hours;
    crew_missing=0;
    time_end_mission_spec=zeros([n_turbines,1]);
    time_restoring=zeros([n_turbines,1]);
    planned_maintenance=zeros([n_turbines, lifetime_years]);
    tot_downtime_sea=zeros([n_turbines,1]);
    MTTF_turbines=ones([1,n_turbines]);
    MTTF_list=[];
    subs_failure_turbine=ones([1,n_turbines]);
    mode_subs_failure=ones([1,n_turbines]);
    number_vessel_rqd=zeros([n_turbines,1]);
    max_vessel_avlb=nr_vessel_UP;
    CREW_req_avlb=nr_crew_UP;
    tot_downtime_spare=zeros([n_turbines,1]);
    tot_downtime_spec_vessel=zeros([n_turbines,1]);
    tot_downtime_org=zeros([n_turbines,1]);
    tot_downtime_repair=zeros([n_turbines,1]);
    tot_downtime_vessel=zeros([n_turbines,1]);
    tot_downtime_vesselmax=zeros([n_turbines,1]);
    tot_downtime_wind=zeros([n_turbines,1]);
    tot_downtime_crew_CTV=zeros([n_turbines,1]);
    tot_downtime_crew_JUV=zeros([n_turbines,1]);
    tot_downtime_crewmax=zeros([n_turbines,1]);
    tot_downtime_CREW_rest=zeros([n_turbines,1]);
    tot_downtime_trtime=zeros([n_turbines,1]);
    tot_downtime_JUV_campaign=zeros([n_turbines,1]);
    tot_downtime_CTV_campaign=zeros([n_turbines,1]);
    ves_req=zeros([n_turbines,1]);
    CREW_req=zeros([n_turbines,1]);
    nbr_shifts_required=zeros([n_turbines,1]);
    spec_ves_req=zeros([n_turbines,1]);
    it=1;
    tot_downtime_BOP_1=0;
    
    tot_downtime_BOP_2=0;
    
    count_planned=0;
    
    ves_req_prev=zeros([n_turbines,1]);
    starting_seastate_power=zeros([n_turbines,1]);
    ending_seastate_power=zeros([n_turbines,1]);
    
    
    %BOP helpers variables
    [r_BOP,c_BOP]=size(failure_rate_BOP);
    MTTF_subsystem_BOP=ones([r_BOP,n_BOP]);
    MTTF_subs_modes_BOP=ones([r_BOP,c_BOP,n_BOP]);
    time_BOP=zeros([n_BOP,1]);
    tot_downtime_spare_BOP=zeros([n_BOP,1]);
    tot_downtime_org_BOP=zeros([n_BOP,1]);
    tot_downtime_repair_BOP=zeros([n_BOP,1]);
    tot_downtime_sea_BOP=zeros([n_BOP,1]);
    tot_downtime_wind_BOP=zeros([n_BOP,1]);
    
    %cost variables
    vessels_cost_up=0; %vessel cost, unplanned maintenance (up). counts the cost and adds up in every mission
    mobilisation_cost_up=0;
    demobilisation_cost_up=0;
    crew_cost_up=0;
    material_cost_up=0;
    material_cost_p=0; %material cost, planned maintenace (p)
    crew_cost_p=0;
    vessel_cost_p=0;  
    energy_cost_up=0;
    energy_cost_p=0;
    energy_prod_revenue=0;
    npvtable{itd}=zeros(lifetime_years+1,4);
    npvtable{itd}(:,1)=1:26;
    
    
    time_turbines=zeros([n_turbines,1]);
    b_turbine=(time_turbines>lifetime_hours);
    test_turbine=1;
    
    %the downtime related to the BOP is considered indipendent from the
    %turbines that are connected to it. For this reason anticipating its calculation
    %has been possible through the use of an external function that relies on
    %the same principles used in the calculation of the turbines unplanned downtimes.
    %Two main outcomes are produced: the times when the BOP fails and the
    %duration of the single BOP failure.
    
    [time_abs_BOP,tot_downtime_BOP]=BOP_life(w_forecast); %(JW: In this function we should introduce the corrections according to corrective maintenance as well) 
    
    %the following are additional values that help proving the accuracy of
    %the BOP downtime prediction in the unplanned failure part.
    
    somma_prova_BOP_1=sum(tot_downtime_BOP(1,:))*(n_turbines/2);
    somma_prova_BOP_2=sum(tot_downtime_BOP(1,:))*(n_turbines/2);
    
    tot_downtime_BOP_prova=somma_prova_BOP_1+somma_prova_BOP_2;
    
    [rl,cl]=size(time_abs_BOP);
    
    
    count_positive=0;
    count_zero=0;
    
       for td=1:n_turbines % CYCLE THAT CALCULATES MTTF FOR EVERY TURBINE (%JCHR: Why not creating a function that passes the arguments for each turbine vectorisedly?)
           
           %     calculates lambda ratios (new version)
           [l_hour, l_modes, l_turb_hour]=lamba_ratios(lambda_turbine, td);
           
           [time_turbines, MTTF_subsystem, MTTF_subs_modes, mode_subsystem, mode, MTTF_turbines, subs_failure_turbine, mode_subs_failure] = REL(l_hour, l_modes, l_turb_hour, td, MTTF_subsystem, MTTF_subs_modes, time_turbines, lifetime_hours, MTTF_turbines, subs_failure_turbine, mode_subs_failure, time_step);                        
               
                
            %% CALCULATING THE POWER (JCHR: Power is calculated here just after uptime, represented by MTTF)
                
              [count_positive, count_zero, power_tot,power_tot_year,year_power] = POWER(time_turbines, time_restoring, td, itd, time_step, hub_heigth, cut_in, cut_out, power_curve, iteration, column_wind, w_forecast, r_power, count_zero, count_positive, power_tot,power_scaling_factor,lifetime_hours);
              energy_prod_revenue=energy_prod_revenue+sum(power_tot_year*energy_price*1000./((1+ir).^year_power));   
              npvtable{itd}(year_power+1,[4])=npvtable{itd}(year_power+1,[4])+[(power_tot_year*energy_price*1000./((1+ir).^year_power))'];
       end
    
    % HERE STARTS  THE MAINTENANCE SIMULATION
    while test_turbine==1 %(JW: loop is performed until all turbines have reached lifetime of WF)
        
        
        if sum(b_turbine)==n_turbines
            test_turbine=0;
        else

    if iteration>1
        for d=1:size(MTTF_list,1)
              td=p_turb(MTTF_list(d)); % JW:updating MTTF information for the turbine which mission has ended

           %     calculates lambda ratios (new version)
              [l_hour, l_modes, l_turb_hour]=lamba_ratios(lambda_turbine, td);
              
              [time_turbines, MTTF_subsystem, MTTF_subs_modes, mode_subsystem, mode, MTTF_turbines, subs_failure_turbine, mode_subs_failure] = REL(l_hour, l_modes, l_turb_hour, td, MTTF_subsystem, MTTF_subs_modes, time_turbines, lifetime_hours, MTTF_turbines, subs_failure_turbine, mode_subs_failure, time_step);                        
               
                
            %% CALCULATING THE POWER (JCHR: Power is calculated here just after uptime, represented by MTTF)
                
              [count_positive, count_zero, power_tot,power_tot_year,year_power] = POWER(time_turbines, time_restoring, td, itd, time_step, hub_heigth, cut_in, cut_out, power_curve, iteration, column_wind, w_forecast, r_power, count_zero, count_positive, power_tot,power_scaling_factor,lifetime_hours);
              energy_prod_revenue=energy_prod_revenue+sum(power_tot_year*energy_price*1000/((1+ir).^year_power));
              npvtable{itd}(year_power,[4])=npvtable{itd}(year_power,[4])+[(power_tot_year*energy_price*1000./((1+ir).^year_power))'];
        end   
        MTTF_list=[];
    end      
            
            %PLANNED MAINTENANCE
            pd=1;
            maint_shifts_planned=ceil(maint_times_P/ work_shift);
            for k_years=1:24 %maintenance is scheduled every year
                for pd=1:n_turbines %(JCHR: This would be vectorizable by embedding the following within a function)
                    if time_turbines(pd,1)>k_years*24*365 && planned_maintenance(pd, k_years)==0
                        
                        %pd=find(time_turbines>k_years*24*365 & planned_maintenance(:, k_years)==0);
                        %hereafter downtimes caused by planned maintenance
                        %are calculated
                        %MAIN ASSUMPTION: only repair
                        %times contribute to planned maint.
                        [~, ~, power_lost_planned,power_lost_year_planned,year_power] = POWER(time_turbines+maint_times_P,time_turbines ,pd , itd, time_step, hub_heigth, cut_in, cut_out, power_curve, iteration, column_wind, w_forecast, r_power, count_zero, count_positive, power_lost_planned,power_scaling_factor,lifetime_hours);
                        yearnpv=ceil(time_turbines(pd,1)/8760);
                        time_turbines(pd,1)=time_turbines(pd,1)+maint_times_P; %+ceil(maint_times_P/(work_shift-2*tr_time(required_main_vessel_UP(maint_main_vessel_P))))*tr_time(required_main_vessel_UP(maint_main_vessel_P));
                        time_restoring(pd,1)=time_turbines(pd,1); %JW: important to have time_restoring here as well which is the time when the turbine is up again
                        tot_planned_maintenance(itd,1)=tot_planned_maintenance(itd,1)+maint_times_P; %+ceil(maint_times_P/(work_shift-2*tr_time(required_main_vessel_UP(maint_main_vessel_P))))*tr_time(required_main_vessel_UP(maint_main_vessel_P));
                        %As soon as the maintenance is done, the MTTF are restored to
                        %zero (only in the proper (minor) failure mode)
                        MTTF_subsystem(:,pd)=0;
                        MTTF_subs_modes(:,1,pd)=0;
                        planned_maintenance(pd, k_years)=1;
                        MTTF_list=[MTTF_list; pd];
                        MTTF_list=unique(MTTF_list);
                        
                        %eventtable=[eventtable;{itd,'Planned',yearnpv,pd,'CTV',1,maint_crew_P}];
                        count_planned=count_planned+1; %variable that counts every planned event
                        energy_cost_p=energy_cost_p+sum(power_lost_year_planned*energy_price*1000./((1+ir).^year_power));
                        material_cost_p=material_cost_p+materials_cost_planned/(((1+ir)^yearnpv));
                        crew_cost_p=crew_cost_p+crew_rates*(maint_crew_P*maint_shifts_planned)/(((1+ir)^yearnpv));
                        vessel_cost_p=vessel_cost_p+vessel_dayrates(2)*(maint_shifts_planned)/(((1+ir)^yearnpv));
                        direct_npv_table=materials_cost_planned/(((1+ir)^yearnpv))+crew_rates*(maint_crew_P*maint_shifts_planned)/(((1+ir)^yearnpv))+vessel_dayrates(2)*(maint_shifts_planned)/(((1+ir)^yearnpv));
                        indirect_npv_table=(power_lost_year_planned*energy_price*1000./((1+ir).^year_power));
                        npvtable{itd}(yearnpv,[2])=npvtable{itd}(yearnpv,[2])+[direct_npv_table'];
                        npvtable{itd}(year_power,[3])=npvtable{itd}(year_power,[3])+[indirect_npv_table'];
                    end
                end
            end
            
            for d=1:size(MTTF_list,1)
              td=MTTF_list(d); % JW:updating MTTF information for the turbine which mission has ended

           %     calculates lambda ratios (new version)
              [l_hour, l_modes, l_turb_hour]=lamba_ratios(lambda_turbine, td);
              
              [time_turbines, MTTF_subsystem, MTTF_subs_modes, mode_subsystem, mode, MTTF_turbines, subs_failure_turbine, mode_subs_failure] = REL(l_hour, l_modes, l_turb_hour, td, MTTF_subsystem, MTTF_subs_modes, time_turbines, lifetime_hours, MTTF_turbines, subs_failure_turbine, mode_subs_failure, time_step);                        
%             %% CALCULATING THE POWER              
              [count_positive, count_zero, power_tot] = POWER(time_turbines, time_restoring, td, itd, time_step, hub_heigth, cut_in, cut_out, power_curve, iteration, column_wind, w_forecast, r_power, count_zero, count_positive, power_tot,power_scaling_factor,lifetime_hours);
            end   
            MTTF_list=[];
            
            %BOP SIMULATION
            for bd=1:n_turbines %similar to what has been done in planned maintenance is used also hereafter %(JCHR: This would be vectorizable by embedding the following within a function)
                for ld=1:cl-1
                    if bd>=1 && bd<=(n_turbines/n_BOP)
                        if time_turbines(bd,1)-time_abs_BOP(1,ld)<MTTF_turbines(1,bd) && time_turbines(bd,1)>time_abs_BOP(1,ld)-MTTF_turbines(1,bd)
                            time_turbines(bd,1)=time_turbines(bd,1)+tot_downtime_BOP(1,ld);
                            tot_downtime_BOP_1=tot_downtime_BOP_1+tot_downtime_BOP(1,ld);
                            
                            break
                        end
                    elseif bd>(n_turbines/n_BOP) && bd<=n_turbines
                        if time_turbines(bd,1)-time_abs_BOP(2,ld)<MTTF_turbines(1,bd) && time_turbines(bd,1)>time_abs_BOP(2,ld)-MTTF_turbines(1,bd)
                            time_turbines(bd,1)=time_turbines(bd,1)+tot_downtime_BOP(2,ld);
                            tot_downtime_BOP_2=tot_downtime_BOP_2+tot_downtime_BOP(2,ld);
                            break
                            
                        end
                    end
                end
            end
            
            %Check if lifetime is reached already
             b_turbine=(time_turbines(:,1)>lifetime_hours); %it is checked if the turbine life exceeds the total windfarm life
            
            %UNPLANNED SIMULATION 
            
            ini=1;
            iniz=1;
            first=1;
            first_spec=1;
            
            [time_sort,p_turb]=sort(time_turbines); %repair maintenance is operated in a chronological way.
            subs_sort=subs_failure_turbine(p_turb);  % Matching the rigth failure with its mode to the turbine is very important
            mode_sort=mode_subs_failure(p_turb);
                       
        end


            kd=1;
%             MTTF_list=p_turb(kd);
            
                if time_turbines(p_turb(kd),1)>lifetime_hours % VERY IMPORTANT to take into account only when we are inside the lifetime of the wind farm
                    continue
                end
                
                % the following arrays call the number of crew and vessel
                % required for the specific mission
                CREW_req(kd,1)=required_crew_UP(subs_sort(kd),mode_sort(kd));
                ves_req(kd,1)=required_main_vessel_UP(subs_sort(kd),mode_sort(kd));
                spec_ves_req(kd,1)=required_support_vessel_UP(subs_sort(kd),mode_sort(kd));
                number_vessel_rqd(kd,1)=ceil(CREW_req(kd,1)/vessel_capacity( ves_req(kd,1)));
                nbr_shifts_required(kd,1)=ceil(repair_time(subs_sort(kd),mode_sort(kd))/(work_shift-2*tr_time(ves_req(kd,1))));
                
                          %% Vessel sharing concept for JUV (JW)
         JUV_list=[];
         if ves_req(kd,1)==3
            JUV_campaign_finished=false;

            for n=1:n_turbines %JW: check all turbines to fill JUV list
            ves_req(n,1)=required_main_vessel_UP(subs_sort(n),mode_sort(n));   
            if ves_req(n,1)==3
                if size(JUV_list,1)==0
                    JUV_list=[JUV_list;[n, time_sort(n,1), subs_sort(n), mode_sort(n)]];
%                     disp(['JUV event of turbine ', num2str(p_turb(n)), ' added to JUV list'])
                    continue  
                end
                    
                if time_sort(n,1) < JUV_list(1,2) + mob_time(3)
                   % add failure in JUV list
      
                    JUV_list=[JUV_list;[n, time_sort(n,1), subs_sort(n), mode_sort(n)]];
%                     disp(['JUV event of turbine ', num2str(p_turb(n)), ' added to JUV list'])
                    continue  
             
                else %JW: start JUV campaign
                    start_campaign_time=JUV_list(size(JUV_list,1),2);
%                     disp(['JUV campaign starts'])
                     cost_list=JUV_list;
                    [JUV_list, time_turbines, time_restoring, b_turbine, tot_downtime_vesselmax, tot_downtime_crewmax, crew_missing, ...
                        tot_downtime_spec_vessel, vessel_crew_list, nr_vessel_UP, nr_crew_UP, time_end_mission_prev, tot_downtime_vessel, ...
                        tot_downtime_crew_JUV,tot_downtime_spare, tot_downtime_JUV_campaign, tot_downtime_org, tot_downtime_sea, ...
                        tot_downtime_wind, tot_downtime_trtime, tot_downtime_repair, MTTF_subsystem, MTTF_subs_modes,iteration,...
                        time_end_campaign, prev_campaign_crew, MTTF_list,nbr_shifts_req_JUV_campaign,CREW_req_JUV_campaign]...
                         = JUV_campaign(start_campaign_time, JUV_list, time_sort, subs_sort, ...
                        mode_sort, required_main_vessel_UP,required_support_vessel_UP,vessel_capacity,repair_time,work_shift, tr_time,...
                        max_vessel_avlb,CREW_req_avlb,time_turbines,time_restoring, b_turbine,tot_downtime_vesselmax,tot_downtime_crewmax,...
                        crew_missing,tot_downtime_spec_vessel,vessel_crew_list,nr_vessel_UP,nr_crew_UP,time_end_mission_prev,...
                        tot_downtime_vessel, tot_downtime_crew_JUV,spare_stock_UP,SWT,tot_downtime_spare,tot_downtime_JUV_campaign,...
                        lifetime_hours,mob_time,MOT,tot_downtime_org,time_step,max_wave,max_wind,w_forecast,column_wave,column_wind,...
                        tot_downtime_sea,tot_downtime_wind,tot_downtime_trtime,tot_downtime_repair,MTTF_subsystem,MTTF_subs_modes,...
                        demob_time,required_crew_UP,CREW_req,n_turbines,mode_subsystem,mode,it,iteration,time_end_campaign, ...
                        prev_campaign_crew,p_turb, MTTF_list);
                    JUV_campaign_finished=true;
                    mission_num=mission_num+1;
                    %numbers for specific mission
                    vessels_used_mission=ceil(nbr_shifts_req_JUV_campaign/2);
                    crew_used_mission=CREW_req_JUV_campaign*nbr_shifts_req_JUV_campaign/2;
                    materials_used_mission=(accumarray([cost_list(:,4) cost_list(:,3)],1,[3 7]))';
                    %cumulative numbers
                    vessels_used(1,ves_req(n,1))=vessels_used(1,ves_req(n,1))+vessels_used_mission;
                    crew_used=crew_used+crew_used_mission;
                    campaign_used(1,ves_req(n,1))=campaign_used(1,ves_req(n,1))+1;
                    materials_used=materials_used+ (accumarray([cost_list(:,4) cost_list(:,3)],1,[3 7]))' ;
                    [~, ~, power_lost_tot,power_lost_year,year_power] = POWER(time_restoring(p_turb(cost_list(:,1))), time_sort(cost_list(:,1)),size(cost_list(:,1),1) , itd, time_step, hub_heigth, cut_in, cut_out, power_curve, iteration, column_wind, w_forecast, r_power, count_zero, count_positive, power_lost_tot,power_scaling_factor,lifetime_hours);
                    yearnpv=ceil(start_campaign_time/8760); %convert hours to year campaign was carried out
                    %eventtable=[eventtable;{itd,'Unplanned',yearnpv,{cost_list},'JUV',vessels_used_mission,crew_used_mission}];
                    %calculate cost and add it up to total costs
                    energy_cost_up=energy_cost_up+sum(power_lost_year*energy_price*1000./((1+ir).^year_power));
                    vessels_cost_up=vessels_cost_up+(vessel_dayrates(1,ves_req(n,1))*vessels_used_mission)/((1+ir)^yearnpv); 
                    mobilisation_cost_up=mobilisation_cost_up+(mobilisation_cost(1,ves_req(n,1)))/((1+ir)^yearnpv);
                    demobilisation_cost_up=demobilisation_cost_up+(demobilisation_cost(1,ves_req(n,1)))/((1+ir)^yearnpv);
                    crew_cost_up=crew_cost_up+crew_rates*crew_used_mission/((1+ir)^yearnpv);
                    material_cost_up=material_cost_up+(sum(materials_cost.*materials_used_mission))/((1+ir)^yearnpv);
                    
                    %npv table
                    direct_npv_table=sum((vessel_dayrates(1,ves_req(n,1))*vessels_used_mission)/((1+ir)^yearnpv))+ sum((mobilisation_cost(1,ves_req(n,1)))/((1+ir)^yearnpv))...
                        +sum((demobilisation_cost(1,ves_req(n,1)))/((1+ir)^yearnpv))+crew_rates*crew_used_mission/((1+ir)^yearnpv)+ sum((sum(materials_cost.*materials_used_mission))/((1+ir)^yearnpv)); 
                    indirect_npv_table=(power_lost_year*energy_price*1000./((1+ir).^year_power));
                    npvtable{itd}(yearnpv,[2])=npvtable{itd}(yearnpv,[2])+[direct_npv_table'];
                    npvtable{itd}(year_power,[3])=npvtable{itd}(year_power,[3])+[indirect_npv_table'];
                    break 

                  
                end
            end
           
            end
            if ne(size(JUV_list,1),0) && JUV_campaign_finished==false %JW: in case there is no JUV maintenance event after mobilisation time which starts the campaign automatically
                start_campaign_time=JUV_list(size(JUV_list,1),2);
                cost_list=JUV_list;
                %disp(['JUV campaign starts'])
                [JUV_list, time_turbines, time_restoring, b_turbine, tot_downtime_vesselmax, tot_downtime_crewmax, crew_missing, ...
                    tot_downtime_spec_vessel, vessel_crew_list, nr_vessel_UP, nr_crew_UP, time_end_mission_prev, tot_downtime_vessel,...
                    tot_downtime_crew_JUV,tot_downtime_spare, tot_downtime_JUV_campaign, tot_downtime_org, tot_downtime_sea, ...
                    tot_downtime_wind, tot_downtime_trtime, tot_downtime_repair, MTTF_subsystem, MTTF_subs_modes,iteration,...
                    time_end_campaign, prev_campaign_crew, MTTF_list,nbr_shifts_req_JUV_campaign,CREW_req_JUV_campaign]...
                    = JUV_campaign(start_campaign_time, JUV_list, time_sort, subs_sort, ...
                    mode_sort, required_main_vessel_UP,required_support_vessel_UP,vessel_capacity,repair_time,work_shift, ...
                    tr_time,max_vessel_avlb,CREW_req_avlb,time_turbines,time_restoring, b_turbine,tot_downtime_vesselmax,...
                    tot_downtime_crewmax, crew_missing,tot_downtime_spec_vessel,vessel_crew_list,nr_vessel_UP,nr_crew_UP,...
                    time_end_mission_prev,tot_downtime_vessel, tot_downtime_crew_JUV,spare_stock_UP,SWT,tot_downtime_spare,...
                    tot_downtime_JUV_campaign,lifetime_hours,mob_time,MOT,tot_downtime_org,time_step,max_wave,max_wind,...
                    w_forecast,column_wave,column_wind,tot_downtime_sea,tot_downtime_wind,tot_downtime_trtime,tot_downtime_repair,...
                    MTTF_subsystem,MTTF_subs_modes,demob_time,required_crew_UP,CREW_req,n_turbines,mode_subsystem,mode,it,...
                    iteration,time_end_campaign, prev_campaign_crew,p_turb, MTTF_list);
                JUV_campaign_finished=true;
                mission_num=mission_num+1;
                %outputtable_JUV=[outputtable_JUV; table(itd,'JUV',1, nbr_shifts_req_JUV_campaign,CREW_req_JUV_campaign)];
                %numbers for specific mission
                vessels_used_mission=ceil(nbr_shifts_req_JUV_campaign/2);
                crew_used_mission=CREW_req_JUV_campaign*nbr_shifts_req_JUV_campaign/2;
                materials_used_mission=(accumarray([cost_list(:,4) cost_list(:,3)],1,[3 7]))';
                %cumulative numbers
                vessels_used(1,ves_req(n,1))=vessels_used(1,ves_req(n,1))+vessels_used_mission;
                crew_used=crew_used+crew_used_mission;
                campaign_used(1,ves_req(n,1))=campaign_used(1,ves_req(n,1))+1;
                materials_used=materials_used+ (accumarray([cost_list(:,4) cost_list(:,3)],1,[3 7]))' ;
                [~, ~, power_lost_tot,power_lost_year,year_power] = POWER(time_restoring(p_turb(cost_list(:,1))), time_sort(cost_list(:,1)),size(cost_list(:,1),1) , itd, time_step, hub_heigth, cut_in, cut_out, power_curve, iteration, column_wind, w_forecast, r_power, count_zero, count_positive, power_lost_tot,power_scaling_factor,lifetime_hours);                yearnpv=ceil(start_campaign_time/8760); %convert hours to year campaign was carried out
                yearnpv=ceil(start_campaign_time/8760); %convert hours to year campaign was carried out
                %eventtable=[eventtable;{itd,'Unplanned',yearnpv,{cost_list},'JUV',vessels_used_mission,crew_used_mission}];
                %calculate cost and add it up to total costs
                energy_cost_up=energy_cost_up+sum(power_lost_year*energy_price*1000./((1+ir).^year_power));
                vessels_cost_up=vessels_cost_up+(vessel_dayrates(1,ves_req(n,1))*vessels_used_mission)/((1+ir)^yearnpv); 
                mobilisation_cost_up=mobilisation_cost_up+(mobilisation_cost(1,ves_req(n,1)))/((1+ir)^yearnpv);
                demobilisation_cost_up=demobilisation_cost_up+(demobilisation_cost(1,ves_req(n,1)))/((1+ir)^yearnpv);
                crew_cost_up=crew_cost_up+crew_rates*crew_used_mission/((1+ir)^yearnpv);
                material_cost_up=material_cost_up+(sum(materials_cost.*materials_used_mission))/((1+ir)^yearnpv);
                
                %npv table
                    direct_npv_table=sum((vessel_dayrates(1,ves_req(n,1))*vessels_used_mission)/((1+ir)^yearnpv))+ sum((mobilisation_cost(1,ves_req(n,1)))/((1+ir)^yearnpv))...
                        +sum((demobilisation_cost(1,ves_req(n,1)))/((1+ir)^yearnpv))+crew_rates*crew_used_mission/((1+ir)^yearnpv)+ sum((sum(materials_cost.*materials_used_mission))/((1+ir)^yearnpv)); 
                    indirect_npv_table=(power_lost_year*energy_price*1000./((1+ir).^year_power));
                    npvtable{itd}(yearnpv,[2])=npvtable{itd}(yearnpv, [2])+[direct_npv_table];
                    npvtable{itd}(year_power,[3])=npvtable{itd}(year_power,[3])+[indirect_npv_table'];
            end
         
            
            if JUV_campaign_finished==true
                continue
            end
         end
         
            %% Vessel sharing concept for CTV (JW)
         CTV_list=[];
         if ves_req(kd,1)==2
            CTV_campaign_finished=false;

            for n=1:n_turbines %JW: check all turbines to fill CTV list
            ves_req(n,1)=required_main_vessel_UP(subs_sort(n),mode_sort(n));   
            if ves_req(n,1)==2
                
                if size(CTV_list,1)==0
                    CTV_list=[CTV_list;[n, time_sort(n,1), subs_sort(n), mode_sort(n)]];
%                     disp(['CTV event of turbine ', num2str(p_turb(n)), ' added to CTV list'])
                    continue  
                end
                    
                if time_sort(n,1) < CTV_list(1,2) + 12 %JW: assumption: only if failure is within one hour, otherwise shift too long; check later if assumption makes sense
                   % add failure in JUV list
                   
                   % if size(CTV_list,1)<=9
                    CTV_list=[CTV_list;[n, time_sort(n,1), subs_sort(n), mode_sort(n)]];
%                     disp(['CTV event of turbine ', num2str(p_turb(n)), ' added to CTV list'])
                   % end
                    continue  
             
                else %JW: start CTV campaign
                    start_campaign_time=CTV_list(size(CTV_list,1),2);
                    cost_list=CTV_list;
                     %disp(['CTV campaign starts'])
                    [CTV_list, time_turbines, time_restoring, b_turbine, tot_downtime_vesselmax, tot_downtime_crewmax, crew_missing, ...
                     tot_downtime_spec_vessel, vessel_crew_list, nr_vessel_UP, nr_crew_UP, time_end_mission_prev, tot_downtime_vessel,...
                     tot_downtime_crew_CTV,tot_downtime_spare, tot_downtime_CTV_campaign, tot_downtime_org, tot_downtime_sea, tot_downtime_wind,...
                     tot_downtime_trtime, tot_downtime_repair, MTTF_subsystem, MTTF_subs_modes,iteration,time_end_campaign, prev_campaign_crew,tot_downtime_CREW_rest, MTTF_list,...
                        number_vessel_rqd_CTV_campaign,nbr_shifts_req_CTV_campaign,CREW_req_CTV_campaign ]...
                     = CTV_campaign(start_campaign_time, CTV_list, time_sort, subs_sort, mode_sort, required_main_vessel_UP,...
                     required_support_vessel_UP,vessel_capacity,repair_time,work_shift, tr_time,max_vessel_avlb,CREW_req_avlb,...
                     time_turbines,time_restoring, b_turbine,tot_downtime_vesselmax,tot_downtime_crewmax, crew_missing,tot_downtime_spec_vessel,...
                     vessel_crew_list,nr_vessel_UP,nr_crew_UP,time_end_mission_prev,tot_downtime_vessel, tot_downtime_crew_CTV,spare_stock_UP,SWT,...
                     tot_downtime_spare,tot_downtime_CTV_campaign,lifetime_hours,mob_time,MOT,tot_downtime_org,time_step,max_wave,max_wind,...
                     w_forecast,column_wave,column_wind,tot_downtime_sea,tot_downtime_wind,tot_downtime_trtime,tot_downtime_repair,MTTF_subsystem,...
                     MTTF_subs_modes,demob_time,required_crew_UP,CREW_req,n_turbines,mode_subsystem,mode,it,iteration,time_end_campaign, ...
                     prev_campaign_crew,p_turb,tot_downtime_CREW_rest, MTTF_list);
                    CTV_campaign_finished=true;
                    % disp(['CTV campaign finished'])
                    mission_num=mission_num+1;
                    %xlswrite('missions.xlsx',{itd,'CTV',number_vessel_rqd_CTV_campaign,nbr_shifts_req_CTV_campaign,CREW_req_CTV_campaign},1,['A',num2str(mission_num+1)])
                   %outputtable_CTV=[outputtable_CTV; table(itd,'CTV',number_vessel_rqd_CTV_campaign,nbr_shifts_req_CTV_campaign,CREW_req_CTV_campaign)];
                   %numbers for specific mission
                vessels_used_mission=nbr_shifts_req_CTV_campaign;
                crew_used_mission=CREW_req_CTV_campaign*nbr_shifts_req_CTV_campaign;
                materials_used_mission=(accumarray([cost_list(:,4) cost_list(:,3)],1,[3 7]))';
                %cumulative numbers
                vessels_used(1,ves_req(n,1))=vessels_used(1,ves_req(n,1))+vessels_used_mission;
                crew_used=crew_used+crew_used_mission;
                campaign_used(1,ves_req(n,1))=campaign_used(1,ves_req(n,1))+1;
                materials_used=materials_used+ (accumarray([cost_list(:,4) cost_list(:,3)],1,[3 7]))' ;
                [~, ~, power_lost_tot,power_lost_year,year_power] = POWER(time_restoring(p_turb(cost_list(:,1))), time_sort(cost_list(:,1)),size(cost_list(:,1),1) , itd, time_step, hub_heigth, cut_in, cut_out, power_curve, iteration, column_wind, w_forecast, r_power, count_zero, count_positive, power_lost_tot,power_scaling_factor,lifetime_hours);
                yearnpv=ceil(start_campaign_time/8760); %convert hours to year campaign was carried out
                %eventtable=[eventtable;{itd,'Unplanned',yearnpv,{cost_list},'CTV',vessels_used_mission,crew_used_mission}];
                %calculate cost and add it up to total costs
                energy_cost_up=energy_cost_up+sum(power_lost_year*energy_price*1000/((1+ir).^year_power));
                vessels_cost_up=vessels_cost_up+(vessel_dayrates(1,ves_req(n,1))*vessels_used_mission)/((1+ir)^yearnpv); 
                mobilisation_cost_up=mobilisation_cost_up+(mobilisation_cost(1,ves_req(n,1)))/((1+ir)^yearnpv);
                demobilisation_cost_up=demobilisation_cost_up+(demobilisation_cost(1,ves_req(n,1)))/((1+ir)^yearnpv);
                crew_cost_up=crew_cost_up+crew_rates*crew_used_mission/((1+ir)^yearnpv);
                material_cost_up=material_cost_up+(sum(materials_cost.*materials_used_mission))/((1+ir)^yearnpv);
                
                %npv table
                 direct_npv_table=sum((vessel_dayrates(1,ves_req(n,1))*vessels_used_mission)/((1+ir)^yearnpv))+ sum((mobilisation_cost(1,ves_req(n,1)))/((1+ir)^yearnpv))...
                        +sum((demobilisation_cost(1,ves_req(n,1)))/((1+ir)^yearnpv))+crew_rates*crew_used_mission/((1+ir)^yearnpv)+ sum((sum(materials_cost.*materials_used_mission))/((1+ir)^yearnpv)); 
                 indirect_npv_table=(power_lost_year*energy_price*1000./((1+ir).^year_power));
                 npvtable{itd}(yearnpv,[2])=npvtable{itd}(yearnpv,[2])+[direct_npv_table'];
                 npvtable{itd}(year_power,[3])=npvtable{itd}(year_power,[3])+[indirect_npv_table'];
                   break 

                  
                end
            end
             end
            
            if ne(size(CTV_list,1),0) && CTV_campaign_finished==false %JW: in case there is no CTV maintenance event after work_shift time which starts the campaign automatically
                start_campaign_time=CTV_list(size(CTV_list,1),2);
                cost_list=CTV_list;
               % disp(['CTV campaign starts'])
               [CTV_list, time_turbines, time_restoring, b_turbine, tot_downtime_vesselmax, tot_downtime_crewmax, crew_missing, ...
                tot_downtime_spec_vessel, vessel_crew_list, nr_vessel_UP, nr_crew_UP, time_end_mission_prev, tot_downtime_vessel,...
                tot_downtime_crew_CTV,tot_downtime_spare, tot_downtime_CTV_campaign, tot_downtime_org, tot_downtime_sea, tot_downtime_wind,...
                tot_downtime_trtime, tot_downtime_repair, MTTF_subsystem, MTTF_subs_modes,iteration,time_end_campaign, prev_campaign_crew,tot_downtime_CREW_rest, MTTF_list,...
                number_vessel_rqd_CTV_campaign,nbr_shifts_req_CTV_campaign,CREW_req_CTV_campaign]...
                = CTV_campaign(start_campaign_time, CTV_list, time_sort, subs_sort, mode_sort, required_main_vessel_UP,...
                required_support_vessel_UP,vessel_capacity,repair_time,work_shift, tr_time,max_vessel_avlb,CREW_req_avlb,...
                time_turbines,time_restoring, b_turbine,tot_downtime_vesselmax,tot_downtime_crewmax, crew_missing,tot_downtime_spec_vessel,...
                vessel_crew_list,nr_vessel_UP,nr_crew_UP,time_end_mission_prev,tot_downtime_vessel, tot_downtime_crew_CTV,spare_stock_UP,SWT,...
                tot_downtime_spare,tot_downtime_CTV_campaign,lifetime_hours,mob_time,MOT,tot_downtime_org,time_step,max_wave,max_wind,...
                w_forecast,column_wave,column_wind,tot_downtime_sea,tot_downtime_wind,tot_downtime_trtime,tot_downtime_repair,MTTF_subsystem,...
                MTTF_subs_modes,demob_time,required_crew_UP,CREW_req,n_turbines,mode_subsystem,mode,it,iteration,time_end_campaign, ...
                prev_campaign_crew,p_turb,tot_downtime_CREW_rest, MTTF_list);
                CTV_campaign_finished=true;
               % disp(['CTV campaign finished'])
               mission_num=mission_num+1;
               %xlswrite('missions.xlsx',{itd,'CTV',number_vessel_rqd_CTV_campaign,nbr_shifts_req_CTV_campaign,CREW_req_CTV_campaign},1,['A',num2str(mission_num+1)])
               %outputtable_CTV=[outputtable_CTV; table(itd,'CTV',number_vessel_rqd_CTV_campaign,nbr_shifts_req_CTV_campaign,CREW_req_CTV_campaign)];
                %numbers for specific mission
                vessels_used_mission=nbr_shifts_req_CTV_campaign;
                crew_used_mission=CREW_req_CTV_campaign*nbr_shifts_req_CTV_campaign;
                materials_used_mission=(accumarray([cost_list(:,4) cost_list(:,3)],1,[3 7]))';
                %cumulative numbers
                vessels_used(1,ves_req(n,1))=vessels_used(1,ves_req(n,1))+vessels_used_mission;
                crew_used=crew_used+crew_used_mission;
                campaign_used(1,ves_req(n,1))=campaign_used(1,ves_req(n,1))+1;
                materials_used=materials_used+ (accumarray([cost_list(:,4) cost_list(:,3)],1,[3 7]))' ;
                [~, ~, power_lost_tot,power_lost_year,year_power] = POWER(time_restoring(p_turb(cost_list(:,1))), time_sort(cost_list(:,1)),size(cost_list(:,1),1) , itd, time_step, hub_heigth, cut_in, cut_out, power_curve, iteration, column_wind, w_forecast, r_power, count_zero, count_positive, power_lost_tot,power_scaling_factor,lifetime_hours);
                yearnpv=ceil(start_campaign_time/8760); %convert hours to year campaign was carried out
                %eventtable=[eventtable;{itd,'Unplanned',yearnpv,{cost_list},'CTV',vessels_used_mission,crew_used_mission}];
                %calculate cost and add it up to total costs
                energy_cost_up=energy_cost_up+sum(power_lost_year*energy_price*1000/((1+ir).^year_power));
                vessels_cost_up=vessels_cost_up+(vessel_dayrates(1,ves_req(n,1))*vessels_used_mission)/((1+ir)^yearnpv); 
                mobilisation_cost_up=mobilisation_cost_up+(mobilisation_cost(1,ves_req(n,1)))/((1+ir)^yearnpv);
                demobilisation_cost_up=demobilisation_cost_up+(demobilisation_cost(1,ves_req(n,1)))/((1+ir)^yearnpv);
                crew_cost_up=crew_cost_up+crew_rates*crew_used_mission/((1+ir)^yearnpv);
                material_cost_up=material_cost_up+(sum(materials_cost.*materials_used_mission))/((1+ir)^yearnpv);
                
                %npv table
                direct_npv_table=sum((vessel_dayrates(1,ves_req(n,1))*vessels_used_mission)/((1+ir)^yearnpv))+ sum((mobilisation_cost(1,ves_req(n,1)))/((1+ir)^yearnpv))...
                        +sum((demobilisation_cost(1,ves_req(n,1)))/((1+ir)^yearnpv))+crew_rates*crew_used_mission/((1+ir)^yearnpv)+ sum((sum(materials_cost.*materials_used_mission))/((1+ir)^yearnpv)); 
                indirect_npv_table=(power_lost_year*energy_price*1000./((1+ir).^year_power));
                npvtable{itd}(yearnpv,[2])=npvtable{itd}(yearnpv,[2])+[direct_npv_table'];
                npvtable{itd}(year_power+1,[3])=npvtable{itd}(year_power,[3])+[indirect_npv_table'];
            end
         
            
            if CTV_campaign_finished==true
                continue
            end
         end

    end

    
  %Outputs are calculated in this final lines of the code  
    
    sum_downtime_fianle(itd,:)=([sum(tot_downtime_org),sum(tot_downtime_repair),sum(tot_downtime_sea),sum(tot_downtime_wind),sum(tot_downtime_spare),sum(tot_downtime_vessel),sum(tot_downtime_crew_CTV),sum(tot_downtime_crew_JUV), sum(tot_downtime_CREW_rest), sum(tot_downtime_trtime), sum(tot_downtime_vesselmax),sum(tot_downtime_crewmax),sum(tot_downtime_JUV_campaign),sum(tot_downtime_CTV_campaign)]);
    DOWNTIME_TOTAL_unplanned(itd,1)=sum(sum_downtime_fianle(itd,:));
    DOWNTIME_TOTAL_BOP(itd,1)=tot_downtime_BOP_1+tot_downtime_BOP_2;
    DOWNTIME_TOTAL(itd,1)=DOWNTIME_TOTAL_unplanned(itd,1)+DOWNTIME_TOTAL_BOP(itd,1)+tot_planned_maintenance(itd,1);
    availability(itd,1)=((n_turbines*lifetime_hours-DOWNTIME_TOTAL(itd,1))/(n_turbines*lifetime_hours))*100;
    availability(itd,2)=itd;
    
    %xlswrite('missions.xlsx',table2cell(outputtable_CTV),'CTV',['A',num2str(itd+1)])
    % xlswrite('missions.xlsx',table2cell(outputtable_JUV),'JUV',['A',num2str(itd+1)])
   % xlswrite('missions.xlsx',{itd,availability(itd,1),sum(power_tot(1,:))},'Final Availability',['A',num2str(itd+1)])
   %[costtable,costtabletxt]=xlsread('missions.xlsx');
    %xlswrite('missions.xlsx',[itd sum(outputtable_CTV{:,[1 3:end]}) sum(outputtable_JUV{:,[1 3:end]})],'Summary',['A',num2str(itd+1)])
    
     %calculate final costs 
%      vessels_cost_total(itd)=sum(vessel_dayrates.*nonzeros(vessels_used)'); %unplanned
%      mobilisation_cost_total(itd)=sum(mobilisation_cost.*nonzeros(campaign_used)');
%      demobilisation_cost_total(itd)=sum(demobilisation_cost.*nonzeros(campaign_used)');
%      crew_cost_total(itd)=crew_rates.*crew_used;
%      material_cost_total(itd)=sum(sum(materials_cost.*materials_used));

      vessels_cost_total(itd)= sum(vessels_cost_up);
      mobilisation_cost_total(itd)=sum(mobilisation_cost_up);
      demobilisation_cost_total(itd)=sum(demobilisation_cost_up);
      crew_cost_total(itd)=crew_cost_up;
      material_cost_total(itd)= sum(material_cost_up);
     
     material_cost_planned_total(itd)=material_cost_p; %planned
     crew_cost_planned_total(itd)=crew_cost_p;
     vessel_cost_planned_total(itd)=vessel_cost_p;
     
     power_lost_cost(itd)=energy_cost_up; %indirect cost
     power_lost_cost_planned(itd)=energy_cost_p;
     
     revenue(itd)=energy_prod_revenue;
     
    
      %write excel output
      xlswrite('Output_Costs.xlsx',{itd,availability(itd,1),sum(power_tot(:,itd)),revenue(itd)},'Final Availability',['A',num2str(itd+1)]);
      xlswrite('Output_Costs.xlsx',{itd,material_cost_total(itd),vessels_cost_total(itd),mobilisation_cost_total(itd),demobilisation_cost_total(itd),crew_cost_total(itd),...
          material_cost_planned_total(itd),vessel_cost_planned_total(itd),crew_cost_planned_total(itd)},'Direct Costs',['A',num2str(itd+1)]);
      xlswrite('Output_Costs.xlsx',{itd,power_lost_cost(itd),power_lost_cost_planned(itd)},'Indirect Costs',['A',num2str(itd+1)]);

end

direct_planned_costs_total=sum([mean(material_cost_planned_total),mean(crew_cost_planned_total),mean(vessel_cost_planned_total)]);
direct_unplanned_costs_total=sum([mean(material_cost_total),mean(vessels_cost_total),mean(mobilisation_cost_total),mean(demobilisation_cost_total),mean(crew_cost_total)]);
direct_costs_total=direct_planned_costs_total+direct_unplanned_costs_total;
indirect_planned_costs_total=mean(power_lost_cost_planned);
indirect_unplanned_costs_total=mean(power_lost_cost);
indirect_costs_total=indirect_planned_costs_total+indirect_unplanned_costs_total;

%average npv table for all iterations
npvtabletemp=cell2mat(npvtable);
%column 1=year, column 2=direct costs, column 3=indirect costs, column
%4=revenue
npvtable_average=[npvtabletemp(:,1) mean(npvtabletemp(:,2:4:size(npvtabletemp,2)),2) mean(npvtabletemp(:,3:4:size(npvtabletemp,2)),2) mean(npvtabletemp(:,4:4:size(npvtabletemp,2)),2)];
npvtable_total=sum(npvtable_average(:,2:end));

%non npv table
nonnpvtable_average=[npvtabletemp(:,1) npvtable_average(:,2:end).*(1+ir).^(1:size(npvtable_average,1))'];
nonnpvtable_total=sum(nonnpvtable_average(:,2:end));

%average final costs
xlswrite('Output_Costs.xlsx',{'Average',mean(availability(:,1)),mean(sum(power_tot)),mean(revenue)},'Final Availability',['A',num2str(itd+3)]);
xlswrite('Output_Costs.xlsx',{'Average',mean(material_cost_total),mean(vessels_cost_total),mean(mobilisation_cost_total),mean(demobilisation_cost_total),mean(crew_cost_total),...
    mean(material_cost_planned_total),mean(crew_cost_planned_total),mean(vessel_cost_planned_total)},'Direct Costs',['A',num2str(itd+3)]);
xlswrite('Output_Costs.xlsx',{'Unplanned costs',direct_unplanned_costs_total},'Direct Costs',['A',num2str(itd+5)]);
xlswrite('Output_Costs.xlsx',{'Planned costs',direct_planned_costs_total},'Direct Costs',['A',num2str(itd+6)]);
xlswrite('Output_Costs.xlsx',{'Total Direct Costs',direct_costs_total},'Direct Costs',['A',num2str(itd+7)]);
xlswrite('Output_Costs.xlsx',{'Average',mean(power_lost_cost),mean(power_lost_cost_planned)},'Indirect Costs',['A',num2str(itd+3)]);
xlswrite('Output_Costs.xlsx',{'Total Indirect Costs',indirect_costs_total},'Indirect Costs',['A',num2str(itd+4)]);
%eventtable.Properties.VariableNames={'Iteration','Event','Year','Turbine','Vessel_Used','Number_of_Vessels','Number_of_Crew'};

for jd=1:n_turbines
    power_tot_finale(jd,1)=(sum(power_tot(jd,:)))/avrg;
end
final_availability=sum(availability(:,1))/avrg; %JCHR:WHY NOT PRESENTING A PDF INSTEAD OF THE MEAN?
disp(['Availability= ',num2str(final_availability),'%'])
% figure(1)
% labels1 = {'Mission organisation','Repair time','Unsuitable weather (sea)','Unsuitable weather (wind)','Spare parts unavailability','Vessel unavailability','Crew unavailability','Crew rest'};
% pie([sum(tot_downtime_org),sum(tot_downtime_repair),sum(tot_downtime_sea),sum(tot_downtime_wind),sum(tot_downtime_spare),sum(tot_downtime_vessel),sum(tot_downtime_crew),tot_downtime_CREW_rest],labels1)
%
% figure(2)
% labels2 = {'Unplanned corrective maintenance','Balance of Plant corrective maintenance','Planned Maintenance'};
% pie([DOWNTIME_TOTAL_unplanned,DOWNTIME_TOTAL_BOP,tot_planned_maintenance],labels2)
% availability=((n_turbines*lifetime_hours-DOWNTIME_TOTAL)/(n_turbines*lifetime_hours))*100;
% disp(['Availability= ',num2str(availability),'%'])
% tot_downtime_BOP_prova;
% DOWNTIME_TOTAL_BOP;
%
figure(1)
bar(power_tot_finale);
title('Energy generated by each wind turbine in its whole life');
ylabel('Energy (GWh)');
xlabel('Turbine');
grid on
%
% tot_failures_yr=sum(count_failures(:))/lifetime_years;
%
% disp(['N.failures/year= ',num2str(tot_failures_yr)])
disp(['Tot. energy produced= ',num2str(sum(power_tot_finale)),' [GWh]'])
% disp(['N.planned maintenance operatios ',num2str(count_planned/25)])
% disp(['N.unplanned BOP maintenance operatios ',num2str(DOWNTIME_TOTAL_BOP/25)])
% % availability_2=((n_turbines*lifetime_hours-(DOWNTIME_TOTAL_unplanned+tot_downtime_BOP_prova))/(n_turbines*lifetime_hours))*100;
% % disp(['Availability= ',num2str(availability_2),'%'])