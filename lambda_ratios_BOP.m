function [l_total_BOP,l_turbine_BOP,l_modes_BOP,l_hour_BOP,l_turb_hour_BOP] = lambda_ratios_BOP(lambda)
%It calculates all lambda ratios

l_modes_BOP = lambda/(365*24);
l_total_BOP = sum(lambda,2);
l_hour_BOP = l_total_BOP./(365*24);

l_turbine_BOP = sum(l_total_BOP); % so we are assuming a serialised system
l_turb_hour_BOP=l_turbine_BOP./(365*24);