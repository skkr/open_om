function [time_turbines, MTTF_subsystem, MTTF_subs_modes, mode_subsystem, mode, MTTF_turbines, subs_failure_turbine, mode_subs_failure] = REL(l_hour, l_modes, l_turb_hour, td, MTTF_subsystem, MTTF_subs_modes, time_turbines, lifetime_hours, MTTF_turbines, subs_failure_turbine, mode_subs_failure, time_step)

%function [time_turbines, MTTF_subsystem, MTTF_subs_modes, mode_subsystem, mode, subs_failure_turbine, mode_subs_failure, MTTF_turbines] = REL(lambda, td, MTTF_subsystem, MTTF_subs_modes, time_turbines, lifetime_hours, subs_failure_turbine, mode_subs_failure, MTTF_turbines, l_mih, l_hour, l_turb_hour, r, c)

    try
      

%         [~,~,l_modes,l_hour,l_turb_hour]=lamba_ratios(lambda);
%         %hour 
            MTTF_aver_hours_tot = 1/l_turb_hour;
            %days
            MTTF_aver_final_days_tot=sum(MTTF_aver_hours_tot)/24;
            %years
            MTTF_years_tot=MTTF_aver_final_days_tot/365;

            %[r,c]=size(lambda);



             MTTF_subsystem(:,td) = MTTF_subsystem(:,td) + MTTF_aver_hours_tot; %JW: not used as only time_turbines is updated for turbines which have MTTF=0

                % THE PROBABILITY THAT A SINGLE SUBSYSTEM IS THE CAUSE
                % OF THE TURBIN'S FAILURE IS CALCULATED THROUGH THE
                % EXPONENTIAL CUMULATIVE DISTRIBUTION
                %P_failure_subsystem(kd,td)=cumulative(l_hour(kd,1),MTTF_subsystem(kd,td)); %JCHR: Prob of failure is just applying the exponential formula!
              %P_failure_subsystem(:,td)=cumulative(l_hour(:,1),MTTF_subsystem(:,td));

                %ONCE THE PROBABILITY HAS BEEN CALCULATED, THE  TIME
                %ARRAY OF THE LIFE OF EACH SUBSYSTEM IS CONSTRUCTED
                %THROUGH AN ADDITON. THE SUBSYSTEM IS IN AN UP STATE
                %ONLY DURING A TIME THAT CORRISPOND TO THE MTTF

            MTTF_subs_modes(:,:,td)=MTTF_subs_modes(:,:,td)+MTTF_aver_hours_tot; %JW: not used as only time_turbines is updated for turbines which have MTTF=0
      

            %%probability to subsystem x to fail
            smp_fail_times = exprnd(1./l_hour);
            mode_subsystem = find (smp_fail_times == min(smp_fail_times));
 
             
%             %hereafter the failure mode is randomly selected taking into account
%             %probabilities as weights
            smp_fail_times_modes = exprnd(1./l_modes(mode_subsystem,:));
            mode = find (smp_fail_times_modes == min(smp_fail_times_modes)); % 1=minor, 2=major, 3=replacement
            
            
            subs_failure_turbine(1,td)=mode_subsystem; 
            mode_subs_failure(1,td)=mode;
            
            %creating matrices for all the turbines

%             MTTF_turbines(1,td)=MTTF_aver_hours_tot; 
%             subs_failure_turbine(1,td)=mode_subsystem; %for this time every failed subsystem and mode of failing are assigned to the corresponding turbine
%             mode_subs_failure(1,td)=mode;

            time_turbines(td,1) = time_turbines(td,1) +smp_fail_times(mode_subsystem);% MTTF_turbines(1,td); %smp_fail_times_modes(mode)instead MTTF seems to be desired but adds too big times for replacements and major failures, so they never appear; %the second important array is the one that takes into account the actual time of the life in which the turbine stands. This is composed by MTTF and downtimes.
            if time_turbines(td,1)<time_step
                time_turbines(td,1)=time_step;
            end
            %JCHR> Include MTTF plus DelayTime instead of MTTF
            if  time_turbines(td,1)>lifetime_hours  % cycle that takes into account only the lifetime  selected by the user
                time_turbines(td,1)=lifetime_hours+1;
            end
            
    catch ME
        
        rethrow(ME)
        
    end
end
