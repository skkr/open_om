function index = random_index(prob) % Function to generate the index randomly depending on certain probabilities for each index (prob vector)

r = rand; % Creates a random number between 0 and 1
prob = cumsum(prob); % Each element of the vector is the cumulative sum of all elements up to that point
index = find(r<=prob,1,'first');

end