function [JUV_list, time_turbines, time_restoring, b_turbine, tot_downtime_vesselmax, tot_downtime_crewmax, crew_missing, ...
    tot_downtime_spec_vessel, vessel_crew_list, nr_vessel_UP, nr_crew_UP, time_end_mission_prev, tot_downtime_vessel,...
    tot_downtime_crew_JUV,tot_downtime_spare, tot_downtime_JUV_campaign, tot_downtime_org, tot_downtime_sea, tot_downtime_wind,...
    tot_downtime_trtime, tot_downtime_repair, MTTF_subsystem, MTTF_subs_modes,iteration,time_end_campaign, prev_campaign_crew, MTTF_list,...
    nbr_shifts_req_JUV_campaign,CREW_req_JUV_campaign]...
    = JUV_campaign(start_campaign_time, JUV_list, time_sort, subs_sort, mode_sort, required_main_vessel_UP,...
    required_support_vessel_UP,vessel_capacity,repair_time,work_shift, tr_time,max_vessel_avlb,CREW_req_avlb,...
    time_turbines,time_restoring, b_turbine,tot_downtime_vesselmax,tot_downtime_crewmax, crew_missing,tot_downtime_spec_vessel,...
    vessel_crew_list,nr_vessel_UP,nr_crew_UP,time_end_mission_prev,tot_downtime_vessel, tot_downtime_crew_JUV,spare_stock_UP,SWT,...
    tot_downtime_spare,tot_downtime_JUV_campaign,lifetime_hours,mob_time,MOT,tot_downtime_org,time_step,max_wave,max_wind,...
    w_forecast,column_wave,column_wind,tot_downtime_sea,tot_downtime_wind,tot_downtime_trtime,tot_downtime_repair,MTTF_subsystem,...
    MTTF_subs_modes,demob_time,required_crew_UP,CREW_req,n_turbines,mode_subsystem,mode,it,iteration,time_end_campaign, ...
    prev_campaign_crew,p_turb, MTTF_list)

%JW: assumption: no specific vessel in jack-up vessel campaign utilised

  tot_CREW_req_JUV_campaign=[];
  ves_req_JUV_campaign=0;
  total_repair_time_JUV_campaign=0;
  nbr_shifts_req_JUV_campaign=0;
  CREW_req_JUV_campaign=0;

  for kd=1:size(JUV_list,1) %JW: to reference back to the original turbine in this function JUV(kd,1) instead p_turb(kd) needs to be used 
                
        % the following arrays call the number of crew and vessel
        % required for the specific mission
        CREW_req(JUV_list(kd,1),1)=required_crew_UP(subs_sort(JUV_list(kd,1)),mode_sort(JUV_list(kd,1)));
        ves_req(JUV_list(kd,1),1)=required_main_vessel_UP(subs_sort(JUV_list(kd,1)),mode_sort(JUV_list(kd,1)));
        spec_ves_req(JUV_list(kd,1),1)=required_support_vessel_UP(subs_sort(JUV_list(kd,1)),mode_sort(JUV_list(kd,1)));
        number_vessel_rqd(JUV_list(kd,1),1)=ceil(CREW_req(JUV_list(kd,1),1)/vessel_capacity( ves_req(JUV_list(kd,1),1)));
        nbr_shifts_required(JUV_list(kd,1),1)=ceil(repair_time(subs_sort(JUV_list(kd,1)),mode_sort(JUV_list(kd,1)))/(work_shift-2*tr_time(ves_req(JUV_list(kd,1),1))));

        %for the JUV campaign 
        tot_CREW_req_JUV_campaign=[tot_CREW_req_JUV_campaign;  CREW_req(JUV_list(kd,1),1)];
        ves_req_JUV_campaign=3;
        total_repair_time_JUV_campaign=total_repair_time_JUV_campaign+repair_time(subs_sort(JUV_list(kd,1)),mode_sort(JUV_list(kd,1)));
        nbr_shifts_req_JUV_campaign=ceil(total_repair_time_JUV_campaign/work_shift); %JW: no travel time considered as break can be on JUV
        CREW_req_JUV_campaign=max(tot_CREW_req_JUV_campaign)*2;
  end
                
%%                 
%                 %the following cycles simulate the ending of the
%                 %last JUV campaign, updates number of crew
%                 %and vessel available.

            %% UP OPTIMISATION (SK&JW)
            %JW: simplified for JUV campaign case. Either vessel in
            %campaign or available

                %if statement to check if we have enough vessels and/or
                %crew in total
                if max_vessel_avlb(3)< 1 || CREW_req_avlb(2)<CREW_req_JUV_campaign
                    if max_vessel_avlb(3)< 1 %check first if we have enough vessels
                    for kd=1:size(JUV_list,1)
                    time_turbines(p_turb(JUV_list(kd,1)),1)=lifetime_hours;
                    time_restoring(p_turb(JUV_list(kd,1)),1)=time_turbines(p_turb(JUV_list(kd,1)),1);
                    b_turbine=(time_turbines(:,1)>=lifetime_hours); %it is checked if the turbine life exceeds the total windfarm life
                    tot_downtime_vesselmax(p_turb(JUV_list(kd,1)),1)=tot_downtime_vesselmax(p_turb(JUV_list(kd,1)),1)+abs(time_sort(JUV_list(kd,1),1)-lifetime_hours);
                    disp('not enough vessels')
                         MTTF_list=[]; 
                         MTTF_list=JUV_list(:,1);
                        

                         it=it+1;

                         b_turbine=(time_turbines(:,1)>lifetime_hours); %it is checked if the turbine life exceeds the total windfarm life
                         sum(b_turbine);
                         iteration=iteration+1;
                    end
                    return
                    elseif CREW_req_avlb(2)<CREW_req_JUV_campaign %enough vessels but not enough crew
                        for kd=1:size(JUV_list,1)
                        time_turbines(p_turb(JUV_list(kd,1)),1)=lifetime_hours;
                        time_restoring(p_turb(JUV_list(kd,1)),1)=time_turbines(p_turb(JUV_list(kd,1)),1);
                        b_turbine=(time_turbines(:,1)>=lifetime_hours); %it is checked if the turbine life exceeds the total windfarm life
                        tot_downtime_crewmax(p_turb(JUV_list(kd,1)),1)=tot_downtime_crewmax(p_turb(JUV_list(kd,1)),1)+abs(time_sort(JUV_list(kd,1),1)-lifetime_hours);
                        crew_missing=abs(nr_crew_UP(2)-CREW_req_JUV_campaign);
                        disp('not enough crew')
                             MTTF_list=[]; 
                             MTTF_list=JUV_list(:,1);
                           

                             it=it+1;

                             b_turbine=(time_turbines(:,1)>lifetime_hours); %it is checked if the turbine life exceeds the total windfarm life
                             sum(b_turbine);
                             iteration=iteration+1;
                        end
                        return
                    end
                end
                

            if time_end_campaign <= time_sort(JUV_list(size(JUV_list,1),1),1)
                nr_vessel_UP(3)=nr_vessel_UP(3)+1;
                nr_crew_UP(2)=nr_crew_UP(2)+prev_campaign_crew;
            end

            if nr_vessel_UP(3) <1
                for kd=1:size(JUV_list,1)
                    time_turbines(p_turb(JUV_list(kd,1)),1)=time_turbines(p_turb(JUV_list(kd,1)),1)+max(time_end_campaign-time_turbines(p_turb(JUV_list(kd,1)),1),0);
                    tot_downtime_vessel(p_turb(JUV_list(kd,1)),1)=tot_downtime_vessel(p_turb(JUV_list(kd,1)),1)+max((time_end_campaign-time_turbines(p_turb(JUV_list(kd,1)),1)),0);
                end
                nr_vessel_UP(3)=nr_vessel_UP(3)+1;
                nr_crew_UP(2)=nr_crew_UP(2)+prev_campaign_crew;
            end

            if nr_crew_UP(2) < CREW_req_JUV_campaign %JW: only taking JUV campaign crew into account. Let's see if we need CTV crew as well
                for kd=1:size(JUV_list,1)
                    time_turbines(p_turb(JUV_list(kd,1)),1)=time_turbines(p_turb(JUV_list(kd,1)),1)+max(time_end_campaign-time_turbines(p_turb(JUV_list(kd,1)),1),0);
                    tot_downtime_crew_JUV(p_turb(JUV_list(kd,1)),1)=tot_downtime_crew_JUV(p_turb(JUV_list(kd,1)),1)+max(time_end_campaign-time_turbines(p_turb(JUV_list(kd,1)),1),0);
                end
                nr_vessel_UP(3)=nr_vessel_UP(3)+1;
                nr_crew_UP(2)=nr_crew_UP(2)+prev_campaign_crew;
            end


            % after if statements enough vessesls and crew ready to go on
            % campaign
                nr_vessel_UP(3)=nr_vessel_UP(3)-1;
                nr_crew_UP(2)=nr_crew_UP(2)-CREW_req_JUV_campaign;
                                  
                
                
                %%
  for kd=1:size(JUV_list,1)
        % Spare stocks downtimes are calculated hereafter. All components
        % need to be available before campaign can start.

        if spare_stock_UP(subs_sort(JUV_list(kd,1)),mode_sort(JUV_list(kd,1)))==0

            for jd=1:size(JUV_list,1)
                if ne(mode_sort(JUV_list(jd,1)),1)
                    time_turbines(p_turb(JUV_list(jd,1)),1)=time_turbines(p_turb(JUV_list(jd,1)),1)+SWT(subs_sort(JUV_list(kd,1)),mode_sort(JUV_list(kd,1)));
                    tot_downtime_spare(p_turb(JUV_list(jd,1)),1)=tot_downtime_spare(p_turb(JUV_list(jd,1)),1)+SWT(subs_sort(JUV_list(kd,1)),mode_sort(JUV_list(kd,1)));
                end
            end
        else
            spare_stock_UP(subs_sort(JUV_list(kd,1)),mode_sort(JUV_list(kd,1)))=spare_stock_UP(subs_sort(JUV_list(kd,1)),mode_sort(JUV_list(kd,1)))-1;
        end
        %eventual spare parts order can be implemented here
  end
  
  for kd=1:size(JUV_list,1)
     
        if time_turbines(p_turb(JUV_list(kd,1)),1)>lifetime_hours % VERY IMPORTANT to take into account only when we are inside the lifetime of the wind farm
            continue
        end

        %Mission organisational downtimes is calculated hereafter

        if ne(spec_ves_req(JUV_list(kd,1),1),0)
            if ne(mode_sort(JUV_list(kd,1)),1)
                time_turbines(p_turb(JUV_list(kd,1)),1)=time_turbines(p_turb(JUV_list(kd,1)),1)+max(mob_time(ves_req(JUV_list(kd,1),1)),mob_time(spec_ves_req(JUV_list(kd,1),1)))-(JUV_list(kd,2)-JUV_list(1,2))+MOT(mode_subsystem,mode);
                tot_downtime_org(p_turb(JUV_list(kd,1)),1)=tot_downtime_org(p_turb(JUV_list(kd,1)),1)+max(mob_time(ves_req(JUV_list(kd,1),1)),mob_time(spec_ves_req(JUV_list(kd,1),1)))-(JUV_list(kd,2)-JUV_list(1,2))+MOT(mode_subsystem,mode);
            end
        else
            if ne(mode_sort(JUV_list(kd,1)),1)
                time_turbines(p_turb(JUV_list(kd,1)),1)=time_turbines(p_turb(JUV_list(kd,1)),1)+mob_time(ves_req(JUV_list(kd,1),1))-(JUV_list(kd,2)-JUV_list(1,2))+MOT(mode_subsystem,mode);
                tot_downtime_org(p_turb(JUV_list(kd,1)),1)=tot_downtime_org(p_turb(JUV_list(kd,1)),1)+mob_time(ves_req(JUV_list(kd,1),1))-(JUV_list(kd,2)-JUV_list(1,2))+MOT(mode_subsystem,mode);
            end
        end
  end
                
        % Downtimes due to weather windows not suitable for safety
        % requirements are calculated in the following  cycles

        %JW: for vessel sharing concept: navigation_time needs to
        %consider the sum of all repair times in JUV_list -->
        %ending seastate later as before
        %downtime same for all turbines in vessel list

        if ne(spec_ves_req(JUV_list(kd,1),1),0)
            navigation_time=ceil(total_repair_time_JUV_campaign+2*max(tr_time(ves_req(JUV_list(kd,1),1)),tr_time(spec_ves_req(JUV_list(kd,1),1))));
        else
            navigation_time=ceil(total_repair_time_JUV_campaign+2*tr_time(ves_req(JUV_list(kd,1),1)));
        end
        starting_seastate=floor(time_turbines(p_turb(JUV_list(kd,1)),1)/time_step);
        ending_seastate=ceil((time_turbines(p_turb(JUV_list(kd,1)),1)+navigation_time)/time_step);
        number_seastate=ending_seastate-starting_seastate+1;
        max_wv=max_wave(ves_req(JUV_list(kd,1),1));
        max_wd=max_wind(ves_req(JUV_list(kd,1),1));
        b_wave=(w_forecast(:,column_wave)<=max_wv);
        b_wind=(w_forecast(:,column_wind)<=max_wd);
        test=1;
        [r1,~]=size(w_forecast);

        while test==1
            ending_seastate=min(lifetime_hours/time_step,ending_seastate); %JW: makes sure we are not running out of bounds of b_wave
            helper_wave=b_wave(starting_seastate:ending_seastate)-ones([ending_seastate-starting_seastate+1,1]);
            helper_wind=b_wind(starting_seastate:ending_seastate)-ones([ending_seastate-starting_seastate+1,1]);

            if ending_seastate>r1
                disp('Sea not safe') % this cycle will never be displayed because the weather simulation is 2 years longer than the windfarm's lifetime, but it is inserted in order to make the code running in every case.
                break                % if displayed can highlight a problem
            end

            if sum(helper_wave)==0 && sum(helper_wind)==0
                test=0;
            elseif sum(helper_wave)~=0 && sum(helper_wind)==0 || sum(helper_wave)~=0 && sum(helper_wind)~=0
                for id=starting_seastate:ending_seastate
                    if w_forecast(id,column_wave)-max_wv>0
                        for kd=1:size(JUV_list,1)
                            if ne(mode_sort(JUV_list(kd,1)),1)
                                time_turbines(p_turb(JUV_list(kd,1)),1)=time_turbines(p_turb(JUV_list(kd,1)),1)+(id+1-starting_seastate)*time_step;
                                tot_downtime_sea(p_turb(JUV_list(kd,1)),1)=tot_downtime_sea(p_turb(JUV_list(kd,1)),1)+(id+1-starting_seastate)*time_step;
                            end
                        end
                        ending_seastate=ending_seastate+id+1-starting_seastate;
                        starting_seastate=starting_seastate+id+1-starting_seastate;
                        break
                    end
                end
            elseif sum(helper_wave)==0 && sum(helper_wind)~=0
                for id=starting_seastate:ending_seastate
                    if w_forecast(id,column_wind)-max_wd>0
                        for kd=1:size(JUV_list,1)
                            if ne(mode_sort(JUV_list(kd,1)),1)
                                time_turbines(p_turb(JUV_list(kd,1)),1)=time_turbines(p_turb(JUV_list(kd,1)),1)+(id+1-starting_seastate)*time_step;
                                tot_downtime_wind(p_turb(JUV_list(kd,1)),1)=tot_downtime_wind(p_turb(JUV_list(kd,1)),1)+(id+1-starting_seastate)*time_step;
                            end
                        end
                        ending_seastate=ending_seastate+id+1-starting_seastate;
                        starting_seastate=starting_seastate+id+1-starting_seastate;
                        break
                    end
                end
            end

        end
                
                %when the weather window has been checked and approved, the
                %mission can take the sea
                
  for kd=1:size(JUV_list,1)
                
        %JW: vessel sharing concept: downtime due to travel time
        %same for all turbines as campaign downtime accounts for the
        %waiting time until mission at turbine starts

        if ne(spec_ves_req(JUV_list(kd,1),1),0)
            if ne(mode_sort(JUV_list(kd,1)),1)
                time_turbines(p_turb(JUV_list(kd,1)),1)=time_turbines(p_turb(JUV_list(kd,1)),1)+max(tr_time(ves_req(JUV_list(kd,1),1)),tr_time(spec_ves_req(JUV_list(kd,1),1)));
                tot_downtime_trtime(p_turb(JUV_list(kd,1)),1)=tot_downtime_trtime(p_turb(JUV_list(kd,1)),1) + max(tr_time(ves_req(JUV_list(kd,1),1)),tr_time(spec_ves_req(JUV_list(kd,1),1)));
            end
        else
            if ne(mode_sort(JUV_list(kd,1)),1)
                time_turbines(p_turb(JUV_list(kd,1)),1)=time_turbines(p_turb(JUV_list(kd,1)),1)+tr_time(ves_req(JUV_list(kd,1),1));
                tot_downtime_trtime(p_turb(JUV_list(kd,1)),1)=tot_downtime_trtime(p_turb(JUV_list(kd,1)),1) + tr_time(ves_req(JUV_list(kd,1),1));
            end
        end


        %at this stage the mission has reached the turbine offshore and the
        %maintenance procedure can start
                
        time_turbines(p_turb(JUV_list(kd,1)),1)=time_turbines(p_turb(JUV_list(kd,1)),1)+repair_time(subs_sort(JUV_list(kd,1)),mode_sort(JUV_list(kd,1))); %JW: check if crew rest downtime is considered above in new part already
        tot_downtime_repair(p_turb(JUV_list(kd,1)),1)=tot_downtime_repair(p_turb(JUV_list(kd,1)),1)+repair_time(subs_sort(JUV_list(kd,1)),mode_sort(JUV_list(kd,1))); %JW: check if crew rest downtime is considered above in new part already

        %JW: for sharing vessel concept: JUV campaign downtime calculated for other turbines in
        %JUV_list which still wait for mission

        for s=(kd+1):size(JUV_list,1) %JW: how to reference back to original turbine?
          time_turbines(p_turb(JUV_list(s,1)),1)=time_turbines(p_turb(JUV_list(s,1)),1) + repair_time(subs_sort(JUV_list(kd,1)),mode_sort(JUV_list(kd,1)));
          tot_downtime_JUV_campaign(p_turb(JUV_list(s,1)),1)=tot_downtime_JUV_campaign(p_turb(JUV_list(s,1)),1) + repair_time(subs_sort(JUV_list(kd,1)),mode_sort(JUV_list(kd,1)));
        end


        %As soon as the maintenance is done, the MTTF are restored to
        %zero (only in the proper failure mode)
        MTTF_subsystem(subs_sort(JUV_list(kd,1)),p_turb(JUV_list(kd,1)))=0;
        MTTF_subs_modes(subs_sort(JUV_list(kd,1)),mode_sort(JUV_list(kd,1)),p_turb(JUV_list(kd,1)))=0;

        if mode_sort(JUV_list(kd,1))==3
            MTTF_subs_modes(subs_sort(JUV_list(kd,1)),:,p_turb(JUV_list(kd,1)))=0;
        end
                
        %A very important parameter is calculated hereafter: the
        %turbine is considered to return to an operative state as soon
        %as the maintenace has been carried out. On the other hand, the
        %vessel and the crew need to come back to the harbour in order
        %to be able to start the next mission. For this reason the time
        %when the turbine is in an upstate differs from the time of the
        %mission by the return-way travel time and the demobilisation
        %time of the vessel.
        
        time_restoring(p_turb(JUV_list(kd,1)),1)=time_turbines(p_turb(JUV_list(kd,1)),1); %JW: is the time when turbine is up again
        
        
  end
                
        time_end_campaign=time_turbines(p_turb(JUV_list(size(JUV_list,1),1)),1)+tr_time(3)+demob_time(3);
        prev_campaign_crew=CREW_req_JUV_campaign;          
               
  %JW: delete entries from JUV_list after campaign has finished but keep
  %turbines which have been maintained in order to update MTTF
     MTTF_list=[]; 
     MTTF_list=JUV_list(:,1);
     JUV_list=[];
 
     it=it+1;

     b_turbine=(time_turbines(:,1)>lifetime_hours); %it is checked if the turbine life exceeds the total windfarm life
     sum(b_turbine);
     iteration=iteration+1;

     % todo: time sort to time restoring power

end