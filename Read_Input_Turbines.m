function [lambda, r, c]=Read_Input_Turbines(td)

% FAILURE RATE INPUT FOR TURBINE GROUPS

sheet_failure_rate = td+1; % Sheet in the "Inputs_failures.xls" file corresponding to failure rate info, in failures per year

lambda=xlsread('Inputs_failure_rates.xlsx',sheet_failure_rate);
[r,c]=size(lambda);


end