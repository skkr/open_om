function [count_positive, count_zero, power_tot,power_tot_year,year_power] = POWER(time_turbines, time_restoring, td, itd, time_step, hub_heigth, cut_in, cut_out, power_curve, iteration, column_wind, w_forecast, r_power, count_zero, count_positive, power_tot,power_scaling_factor,lifetime_hours)
    
    k=1;
    year_power_step(k)=floor(time_restoring(td,1)/8760);
    power_step(k)=0;
    starting_seastate_power(td,1)=floor(time_restoring(td,1)/time_step); %time restoring is when the repair has been completed and the turbine is again in an upstate
    if starting_seastate_power(td,1)==0
        starting_seastate_power(td,1)=1;
    end
    ending_seastate_power(td,1)=ceil(time_turbines(td,1)/time_step); % the end sea state takes into account the MTTF
    if ending_seastate_power(td,1)>=lifetime_hours/time_step
          ending_seastate_power(td,1)=lifetime_hours/time_step;
  end

                
for ud=starting_seastate_power(td,1):ending_seastate_power(td,1) %power is calculated exploiting the weather data simulated before in the code. Every seastate is characterised by a specific wind magnitude and direction 

    alfa=0.37-0.088*log(w_forecast(ud,column_wind));
    wind_speed_hub=w_forecast(ud,column_wind)*(hub_heigth/10)^alfa; %power law
    
     if isnan(wind_speed_hub)
        wind_speed_hub = 0;
        energy=0;
     end
     
     
         if wind_speed_hub<cut_in || wind_speed_hub>cut_out %when the wind speed is too high or too low, the turbine cannot extract power
        pwr_turb=0;
%         count_zero=count_zero+1; % "poor" seastates are counted here
        energy=pwr_turb;
    else % if the wind speed is in the rangeafforded by the turbine, then its direction is checked and multiplied with the corresponding factor.
        for id=1:r_power-1 %this cycle creates the interpolation in the turbine power curve
            if wind_speed_hub>=power_curve(id,1) && wind_speed_hub<power_curve(id+1,1)
                pwr_turb=power_curve(id,2)+(wind_speed_hub-power_curve(id,1))*((power_curve(id+1,2)-power_curve(id,2))/(power_curve(id+1,1)-power_curve(id,1))); % in kW
                energy=pwr_turb*0.000001*time_step*power_scaling_factor(td);% in GWh
            end
            
%             count_positive=count_positive+1; %in the same way as before, "good" sea states are counted
        end
    end

    
    
    power_tot(td,itd)=power_tot(td,itd)+energy; %for each turbine the total power is progressively calculated by the addition of the single seastates' productions
    
    %calculate energy in each time step and save the time for npv
    %calculations
    
    power_step(k)=energy;
    year_power_step(k)=ceil(ud*time_step/8760);
    k=k+1;
    
end

 [year_power,~,yearidx]=unique(year_power_step);
 for j=1:length(year_power)
 power_tot_year(j)=sum(power_step((yearidx==j))); %group energy in same year
 end
 
end