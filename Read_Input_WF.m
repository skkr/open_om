%General information about whole windfarm

work_shift=12;% in hours
n_BOP = 2; % Number of BOP units in the wind farm. Defined by user

sheet_lifetime=2;
sheet_distance=3;
sheet_turbine_number=4;
sheet_turbine_position=5;
sheet_power_scaling_factor=6;
sheet_failure_rate_grouping=7;

lifetime_years = xlsread('Inputs_windfarm.xlsx', sheet_lifetime);
wheather_yrs=lifetime_years+1; 
time_step=3;
lifetime_hours=lifetime_years*365*24;

distance=xlsread('Inputs_windfarm.xlsx', sheet_distance); %distance from shore to centroid in windfarm in km

n_turbines=xlsread('Inputs_windfarm.xlsx', sheet_turbine_number); %number of turbines in WF

turbine_position=xlsread('Inputs_windfarm.xlsx', sheet_turbine_position); %position of turbines given in easting and northing from farm centroid in meters

power_scaling_factor=xlsread('Inputs_windfarm.xlsx', sheet_power_scaling_factor); %percentage of power production due to wake effects

failure_rate_grouping=xlsread('Inputs_windfarm.xlsx', sheet_failure_rate_grouping); %JW: not used in code. More for own visualisation. For each turbine a sheet for failure rates is created. 

%WEATHER DATA

column_wave = 6; % Column in the historic data file (and the forecast matrix) containing wave height information, in meters
column_wind = 5; % Column in the historic data file (and the forecast matrix) containing wind speed information, in meters per second
column_month = 2; % Column in the historic data file (and the forecast matrix) containing month information, in number
column_year = 1; % Column in the historic data file (and the forecast matrix) containing year information
column_hour = 4; % Column in the historic data file (and the forecast matrix) containing information about time of the day, in hours
column_day = 3; % Column in the historic data file (and the forecast matrix) containing day information
column_direction=7; % Column in the historic data file (and the forecast matrix) containing wind direction information
total_columns = 7; % Total number of columns in historic data file and forecast matrix

% DEFINE FAILURE RATES HERE AND MAIN PARAMETERS
sheet_mob_time = 3; % Sheet in the "Inputs_vessel_crew.xlsx" file corresponding to mobilisation times information.
sheet_nr_vessel_UP = 2; % Sheet in the "Inputs_vessel_crew.xlsx" file corresponding to number of vessels available for UnPlanned maintenance.
sheet_max_wave = 6; % Sheet in the "Inputs_vessel_crew.xlsx" file corresponding to maximum allowable wave information.
sheet_max_wind = 7; % Sheet in the "Inputs_vessel_crew.xlsx" file corresponding to maximum allowable wind information.
sheet_tr_time = 4; % Sheet in the "Inputs_vessel_crew.xlsx" file corresponding to travel times information.
sheet_demob_time = 5; % Sheet in the "Inputs_vessel_crew.xlsx" file corresponding to demobilisation times information.
sheet_nr_crew_UP = 8; % Sheet in the "Inputs_vessel_crew.xlsx" file corresponding to number of crew members initially available for UP maintenance.
sheet_nr_vessel_P = 9; % Sheet in the "Inputs_vessel_crew.xlsx" file corresponding to number of vessels additionally available for Planned maintenance.
sheet_nr_crew_P = 10; % Sheet in the "Inputs_vessel_crew.xlsx" file corresponding to number of crew members additionally available for P maintenance.
sheet_vessel_cap = 11; % % Sheet in the "Inputs_vessel_crew.xlsx" file corresponding to crew capacity of vessels(equipment).

nr_vessel_UP = xlsread('Inputs_vessel_crew.xlsx', sheet_nr_vessel_UP); % Number of each type of vessel available initially for UP maintenance
mob_time = xlsread('Inputs_vessel_crew.xlsx', sheet_mob_time); % Mobilisation time of each type of vessel, in hours
mob_time = mob_time + 0.1*(mob_time==0); % All elements equal to zero are turned to 0.1. Otherwise, the simulation might fail
max_wave = xlsread('Inputs_vessel_crew.xlsx', sheet_max_wave); % Max wave height each vessel can take, in meters
max_wind = xlsread('Inputs_vessel_crew.xlsx', sheet_max_wind); % Max wind speed each vessel can take, in meters per second
nr_crew_UP = xlsread('Inputs_vessel_crew.xlsx', sheet_nr_crew_UP); % Number of crew members available initially for UP
tr_time = xlsread('Inputs_vessel_crew.xlsx', sheet_tr_time); % Travel time from port to wind farm, in hours
tr_time = tr_time + 0.1*(tr_time==0); % All elements equal to zero are turned to 0.1. Otherwise, the simulation might fail
demob_time = xlsread('Inputs_vessel_crew.xlsx', sheet_demob_time); % De-Mobilisation time of each type of vessel, in hours
demob_time = demob_time + 0.1*(demob_time==0); % All elements equal to zero are turned to 0.1. Otherwise, the simulation might fail
nr_vessel_P = xlsread('Inputs_vessel_crew.xlsx', sheet_nr_vessel_P); % Number of each type of vessel available additionally for P maintenance
nr_crew_P = xlsread('Inputs_vessel_crew.xlsx', sheet_nr_crew_P); % Number of crew members available additionally for P
vessel_capacity = xlsread('Inputs_vessel_crew.xlsx', sheet_vessel_cap); % Capcity of each vessel

vessel_types = size(mob_time,2); % Number of vessel types; 1 being tle lightest vessel for minor repair and 3 the biggest for replacememnt repairs

sheet_failure_rate = 2; % Sheet in the "Inputs_failures.xls" file corresponding to failure rate info, in failures per year
sheet_repair_time = 3; % Sheet in the "Inputs_failures.xls" file corresponding to repair time info, in hours
sheet_required_crew = 4; % Sheet in the "Inputs_failures.xls" file corresponding to required crew number info
sheet_required_main_vessel = 5; % Sheet in the "Inputs_failures.xls" file corresponding to required main vessel type info
sheet_required_support_vessel = 6; % Sheet in the "Inputs_failures.xls" file corresponding to required support vessel type info
sheet_spare_stock = 7; % Sheet in the "Inputs_failures.xls" file corresponding to required spare stock info
sheet_spare_stock_min= 8; % Sheet in the "Inputs_failures.xls" file corresponding to required minimum spare stock info
sheet_spare_wait_time = 9; % Sheet in the "Inputs_failures.xls" file corresponding to required spare wait time info, in hours
sheet_mission_org_time = 10; % Sheet in the "Inputs_failures.xls" file corresponding to mission organisation time info, in hours

% lambda=xlsread('Inputs_failures.xlsx',sheet_failure_rate);
% [r,c]=size(lambda);
repair_time=xlsread('Inputs_failures.xlsx',sheet_repair_time); % Time of actual work required for each failure type of each component in hours
repair_time= repair_time+ 0.1*(repair_time==0); % All elements equal to zero are turned to 0.1. Otherwise, the simulation might fail
SWT = xlsread('Inputs_failures.xlsx',sheet_spare_wait_time); % Wait time of substitute component to be delivered to storage, in hours
SWT = SWT + 0.1*(SWT==0); % All elements equal to zero are turned to 0.1. Otherwise, the simulation might fail
MOT = xlsread('Inputs_failures.xlsx',sheet_mission_org_time); % Matrix with organisational times for each failure of each subsystem
MOT = MOT + 0.1*(MOT==0); % All elements equal to zero are turned to 0.1. Otherwise, the simulation might fail


required_main_vessel_UP = xlsread('Inputs_failures.xlsx',sheet_required_main_vessel); % Type of vessel required for each failure type of each subsystem.
required_support_vessel_UP = xlsread('Inputs_failures.xlsx',sheet_required_support_vessel); % Type of vessel required for each failure type of each subsystem.
required_crew_UP = xlsread('Inputs_failures.xlsx',sheet_required_crew); % Number of crew members required for each failure type of each subsystem
spare_stock_initial_UP = xlsread('Inputs_failures.xlsx',sheet_spare_stock); % Amount of spare parts available in stock at the beginning of the simulation
spare_stock_UP = spare_stock_initial_UP; % Amount of spare parts normally available in stock for instantanious use
spare_min_UP = xlsread('Inputs_failures.xlsx',sheet_spare_stock_min); % Amount of spare parts normally available in stock for instantanious use

% BOP unplanned maintenance inputs

sheet_failure_rate_BOP = 2; % Sheet in the "Inputs_failures_BOP.xls" file corresponding to failure rate info, in failures per year
sheet_repair_time_BOP = 3; % Sheet in the "Inputs_failures_BOP.xls" file corresponding to repair time info, in hours
sheet_required_crew_BOP = 4; % Sheet in the "Inputs_failures_BOP.xls" file corresponding to required crew number info
sheet_required_main_vessel_BOP = 5; % Sheet in the "Inputs_failures_BOP.xls" file corresponding to required main vessel type info
sheet_required_support_vessel_BOP = 6; % Sheet in the "Inputs_failures_BOP.xls" file corresponding to required support vessel type info
sheet_spare_stock_BOP = 7; % Sheet in the "Inputs_failures_BOP.xls" file corresponding to required spare stock info
sheet_spare_stock_min_BOP = 8; % Sheet in the "Inputs_failures_BOP.xls" file corresponding to required minimum spare stock info
sheet_spare_wait_time_BOP = 9; % Sheet in the "Inputs_failures_BOP.xls" file corresponding to required spare wait time info
sheet_mission_org_time_BOP = 10; %% Sheet in the "Inputs_failures_BOP.xls" file corresponding to mission oragnisation time info

failure_rate_BOP = xlsread('Inputs_failures_BOP.xlsx',sheet_failure_rate); % Matrix with the failure rate for each type of failure of each subsystem
required_main_vessel_BOP = xlsread('Inputs_failures_BOP.xlsx',sheet_required_main_vessel); % Type of vessel required for each failure type of each subsystem.
required_support_vessel_BOP = xlsread('Inputs_failures_BOP.xlsx',sheet_required_support_vessel); % Type of vessel required for each failure type of each subsystem.
required_crew_BOP = xlsread('Inputs_failures_BOP.xlsx',sheet_required_crew); % Number of crew members required for each failure type of each subsystem
repair_time_BOP = xlsread('Inputs_failures_BOP.xlsx',sheet_repair_time); % Time of actual work required for each failure type of each component in hours
repair_time_BOP = repair_time_BOP + 0.1*(repair_time_BOP==0); % All elements equal to zero are turned to 0.1. Otherwise, the simulation might fail
spare_wait_BOP = xlsread('Inputs_failures_BOP.xlsx',sheet_spare_wait_time); % Wait time of substitute component to be delivered to storage, in hours
spare_wait_BOP = spare_wait_BOP + 0.1*(spare_wait_BOP==0); % All elements equal to zero are turned to 0.1. Otherwise, the simulation might fail
spare_stock_initial_BOP = xlsread('Inputs_failures_BOP.xlsx',sheet_spare_stock); % Amount of spare parts available in stock at the beginning of the simulation
spare_stock_BOP = spare_stock_initial_BOP; % Amount of spare parts normally available in stock for instantanious use
spare_min_BOP = xlsread('Inputs_failures_BOP.xlsx',sheet_spare_stock_min); % Amount of spare parts normally available in stock for instantanious use
total_failure_rate_BOP = sum(failure_rate_BOP,2); % Vector with the total failure rate of each subsystem
org_time_BOP = xlsread('Inputs_failures_BOP.xlsx',sheet_mission_org_time); % Matrix contaitng info with organisation times for BOP unplanned maintenance
org_time_BOP = org_time_BOP + 0.1*(org_time_BOP==0); % All elements equal to zero are turned to 0.1. Otherwise, the simulation might fail

n_subsystems_BOP = size(failure_rate_BOP,1); % Number of BOP subsystems
n_failures_BOP = size(failure_rate_BOP,2); % Number denoting different types of failures of BOP

% Planned maintenance inputs

sheet_maint_times = 3; % Sheet in the "Inputs_planned_maintenance.xlsx" file corresponding to the times required to do planned maintenance, in hours
sheet_maint_groups = 2; % Sheet in the "Inputs_planned_maintenance.xlsx" file telling which group of planned maintenance does each subsystem belong to.
sheet_maint_schedule = 4; % Sheet in the "Inputs_planned_maintenance.xlsx" file corresponding to times between maintenance interventions for each group of subsystems. In hours.
sheet_maint_crew = 5; % Sheet in the "Inputs_planned_maintenance.xlsx" file with info on the crew number needed for maintaining each group of subsystems
sheet_main_maint_vessel = 6; % Sheet in the "Inputs_planned_maintenance.xlsx" file with info on required vessel type for each maintenance group
sheet_support_maint_vessel = 7; % Sheet in the "Inputs_planned_maintenance.xlsx" file with info on required vessel type for each maintenance group

maint_times_P = xlsread('Inputs_planned_maintenance.xlsx', sheet_maint_times); % Time required to do planned maintenance for each maintenance group of subsystems
maint_times_P = maint_times_P + 0.1*(maint_times_P==0); % All elements equal to zero are turned to 0.1. Otherwise, the simulation might fail
maint_group_P = xlsread('Inputs_planned_maintenance.xlsx', sheet_maint_groups); % Vector that tells what maintenance group each subsystem belongs to
maint_schedule_P = xlsread('Inputs_planned_maintenance.xlsx', sheet_maint_schedule); % Vector with the times between maintenance interventions for each maintenance group.
maint_main_vessel_P = xlsread('Inputs_planned_maintenance.xlsx', sheet_main_maint_vessel); % Vector with the main vessel type required to maintain each group of subsystems
maint_support_vessel_P = xlsread('Inputs_planned_maintenance.xlsx', sheet_support_maint_vessel); % Vector with the support vessel type required to maintain each group of subsystems
maint_crew_P = xlsread('Inputs_planned_maintenance.xlsx', sheet_maint_crew); % Vector with the number of crew members required to maintain each group of subsystems

%POWER LOADING
sheet_power_curve = 2; % Sheet in the 'Inputs_power.xlsx' file with the power (kW) vs wind speed (m/s) curve for the wind turbines
power_curve = xlsread('Inputs_power.xlsx',sheet_power_curve); % Power curve of the turbines in the wind farm
[r_power,~]=size(power_curve);
sheet_cut_in=3;
sheet_cut_out=4;
sheet_hub_height=5;
cut_in=xlsread('Inputs_power.xlsx',sheet_cut_in); %speed in m/s
cut_out=xlsread('Inputs_power.xlsx',sheet_cut_out);%speed in m/s
hub_heigth=xlsread('Inputs_power.xlsx',sheet_hub_height);% hub heigth in meter