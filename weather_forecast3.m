% Weather forecast module

function w_forecast = weather_forecast3(yrs,time_step) % Years (input) refers to the number of years into the future for which to predict

%% SETTINGS

% First the historic weather file is opened
% History=xlsread('History2.xlsx');
History=xlsread('History_fino3_formatted.xlsx');
% History = csvread('History.csv',1,0); % History is a matrix where the data from the csv (comma separated values) file Hystory.csv is stored. This file needs to have at least month (in number), wave height (m) and wind speed (m/s) in different columns separated by commas
column_wave = 6; % Column in the historic data file (and the output matrix) containing wave height information, in meters
column_wind = 5; % Column in the historic data file (and the output matrix) containing wind speed information, in meters per second
column_month = 2; % Column in the historic data file (and the output matrix) containing month information, in number
column_year = 1; % Column in the historic data file (and the output matrix) containing year information
column_hour = 4; % Column in the historic data file (and the output matrix) containing information about time of the day, in hours
column_day = 3; % Column in the historic data file (and the output matrix) containing day information
column_direction=7; % Column in the historic data file (and the output matrix) containing wind direction information
total_columns = 7; % Total number of columns in historic data file and output matrix

wave_resolution = 0.2; % Difference between consecutive wave height values, in meters
min_wave = 0; % Minimum wave height to be considered, in meters
max_wave = max(History(:,column_wave)) + wave_resolution; % Maximum wave height to be considered, in meters

wind_resolution = 1; % Difference between consecutive wind speed values, in meters per second
min_wind = 0; % Minimum wind speed to be considered, in meters per second
max_wind = max(History(:,column_wind)) + wind_resolution; % Maximum wind speed to be considered, in meters per second

direction_resolution=1; %degree
min_direction=0;
max_direction=360;

first_month = 1; % Month of the year when the simulation will start
first_year = 2020; % Year when the simulation will start. Not relevant at this point
first_day = 1; % Day of the first month when the simulation will start
first_hour = 0; % Hour of the first day when the simulation will start


%% PRELIMINARY CALCULATIONS

wave_values = min_wave:wave_resolution:max_wave; % Possible wave height values, in meters
n_wave = size(wave_values,2); % Number of different possible wave height values

wind_values = min_wind:wind_resolution:max_wind; % Possible wind speed values, in meters per second
n_wind = size(wind_values,2); % % Number of different possible wind speed values

direction_values=min_direction:direction_resolution:max_direction;
n_direction=size(direction_values,2);

%% DISCRETISE HISTORIC WEATHER DATA

History_discrete = History; % Matrix where the historic data will be discretised for the values selected before (through minimum, maximum and resolution of wave height and wind speed)

for i=1:size(History_discrete,1) % This loop rounds the historic data (wave height and wind speed) to the closest of the discrete values being considered
    
    j = 1; % Auxiliary variable to check the different possible discrete values of wave height (wave_values)
    round = 0; % Auxiliary variable to exit the while loop when the closest dicrete value has been found
    while round == 0 % This loop does the discretisation for wave height values
        
        if(History_discrete(i,column_wave) < wave_values(j)+0.5*wave_resolution-0.000001) % Small quantity subtracted to avoid rounding problems in the exact middle value. In that case it will always round to the larger value
            History_discrete(i,column_wave) = wave_values(j);
            round = 1;
        else
            j = j+1;
            if(j==n_wave)
                History_discrete(i,column_wave) = wave_values(j);
                round = 1;
            end
        end
        
    end
    
    j = 1; % Auxiliary variable to check the different possible discrete values of wind speed (wind_values)
    round = 0; % Auxiliary variable to exit the while loop when the closest dicrete value has been found
    while round == 0 % This loop does the discretisation for wind speed values
        
        if(History_discrete(i,column_wind) < wind_values(j)+0.5*wind_resolution-0.000001) % Small quantity subtracted to avoid rounding problems in the exact middle vale. In that case it will always round to the larger value
            History_discrete(i,column_wind) = wind_values(j);
            round = 1;
        else
            j = j+1;
            if(j==n_wind)
                History_discrete(i,column_wind) = wind_values(j);
                round = 1;
            end
        end
        
    end
    
    j = 1; % Auxiliary variable to check the different possible discrete values of wind direction direction_values)
    round = 0; % Auxiliary variable to exit the while loop when the closest dicrete value has been found
    while round == 0 % This loop does the discretisation for wind speed values
        
        if(History_discrete(i,column_direction) < direction_values(j)+0.5*direction_resolution-0.000001) % Small quantity subtracted to avoid rounding problems in the exact middle vale. In that case it will always round to the larger value
            History_discrete(i,column_direction) = direction_values(j);
            round = 1;
        else
            j = j+1;
            if(j==n_direction)
                History_discrete(i,column_direction) = direction_values(j);
                round = 1;
            end
        end
        
    end
    
end


%% DETERMINE MARKOV PROBABILITY MATRIXES FOR EACH MONTH (WAVE HEIGHT)

total = zeros(n_wave,12); % Auxiliary matrix that will store how many times each wave height state "happens" for each month

% January

month = 1;
[Prob_Jan, total(:,month)] = Prob_matrix2(month, History_discrete, column_month, column_wave, wave_values, n_wave); % Function to determine probability matrixes

% February

month = 2;
[Prob_Feb, total(:,month)] = Prob_matrix2(month, History_discrete, column_month, column_wave, wave_values, n_wave);

% March

month = 3;
[Prob_Mar, total(:,month)] = Prob_matrix2(month, History_discrete, column_month, column_wave, wave_values, n_wave);

% April

month = 4;
[Prob_Apr, total(:,month)] = Prob_matrix2(month, History_discrete, column_month, column_wave, wave_values, n_wave);

% May

month = 5;
[Prob_May, total(:,month)] = Prob_matrix2(month, History_discrete, column_month, column_wave, wave_values, n_wave);

% June

month = 6;
[Prob_Jun, total(:,month)] = Prob_matrix2(month, History_discrete, column_month, column_wave, wave_values, n_wave);

% July

month = 7;
[Prob_Jul, total(:,month)] = Prob_matrix2(month, History_discrete, column_month, column_wave, wave_values, n_wave);

% August

month = 8;
[Prob_Aug, total(:,month)] = Prob_matrix2(month, History_discrete, column_month, column_wave, wave_values, n_wave);

% September

month = 9;
[Prob_Sep, total(:,month)] = Prob_matrix2(month, History_discrete, column_month, column_wave, wave_values, n_wave);

% October

month = 10;
[Prob_Oct, total(:,month)] = Prob_matrix2(month, History_discrete, column_month, column_wave, wave_values, n_wave);

% November

month = 11;
[Prob_Nov, total(:,month)] = Prob_matrix2(month, History_discrete, column_month, column_wave, wave_values, n_wave);

% December

month = 12;
[Prob_Dec, total(:,month)] = Prob_matrix2(month, History_discrete, column_month, column_wave, wave_values, n_wave);


%% DETERMINE WIND SPEEDS PROBABILITY VECTOR FOR EACH WAVE HEIGHT

Prob_wind = zeros(n_wave,n_wind); % Matrix containing for each wave height (rows) the probability of having each wind speed (columns)
total_wave = zeros(n_wave,1); % Auxiliary array to store the number of times each wave height appears

for i=1:size(History_discrete) % Goes through all the rows of the historic data

    for j=1:n_wave % Checks which of the possible values of wave height is in this row
        
        if(History_discrete(i,column_wave)==wave_values(j))
            
            total_wave(j,1) = total_wave(j,1) + 1; % One unit is added to the number of times this wave height occurs
            
            for k=1:n_wind % Checks what wind speed corresponds to this wave height
                if(History_discrete(i,column_wind)==wind_values(k))
                    Prob_wind(j,k) = Prob_wind(j,k) + 1;
                    break; % Once we find the wind speed, there is no need to keep looking
                end
            end
            
            break % Once we have found the wave height, there is no need to keep looking
            
        end
        
    end
    
end


%% DETERMINE WIND DIRECTIONS PROBABILITY VECTOR FOR EACH WIND SPEED

Prob_direction = zeros(n_wave,n_direction); % Matrix containing for each wave height (rows) the probability of having each wind direction (columns)
total_wave = zeros(n_wave,1); % Auxiliary array to store the number of times each wave height appears

for i=1:size(History_discrete) % Goes through all the rows of the historic data

    for j=1:n_wave % Checks which of the possible values of wave height is in this row
        
        if(History_discrete(i,column_wave)==wave_values(j))
            
            total_wave(j,1) = total_wave(j,1) + 1; % One unit is added to the number of times this wave height occurs
            
            for k=1:n_direction % Checks what wind direction corresponds to this wave height
                if(History_discrete(i,column_direction)==direction_values(k))
                    Prob_direction(j,k) = Prob_direction(j,k) + 1;
                    break; % Once we find the wind direction, there is no need to keep looking
                end
            end
            
            break % Once we have found the wave height, there is no need to keep looking
            
        end
        
    end
    
end
%% 

for i=1:n_wave % This loop "normalises" the probability matrix
   if(total_wave(i,1)>0)
       Prob_wind(i,:) = 1/total_wave(i,1) * Prob_wind(i,:);
   else
       Prob_wind(i,:) = 2; % In the unlikely case that one wave height value has not appeared, a value of 2 is assigned to all its wind speeds. This has no effect, just avoids dividing by 0
   end
end
for i=1:n_wave % This loop "normalises" the probability matrix
   if(total_wave(i,1)>0)
       Prob_direction(i,:) = 1/total_wave(i,1) * Prob_direction(i,:);
   else
       Prob_direction(i,:) = 2; % In the unlikely case that one wave height value has not appeared, a value of 2 is assigned to all its wind directions. This has no effect, just avoids dividing by 0
   end
end


%% FORECAST WEATHER

index_wave = random_index(total(:,first_month)/sum(total(:,first_month))); % Determines the index of the wave height where the simulation starts. It is generated randomly using the probabilities of each wave height value in the first month
initial_wave = wave_values(index_wave); % Initial wave height
index_wind = random_index(Prob_wind(index_wave,:)); % Generates random index that will determine wind speed for the initial step. It depends on the probability of each wind speed for the initial wave height
initial_wind = wind_values(index_wind); % Initial wind speed
index_direction = random_index(Prob_direction(index_wave,:)); % Generates random index that will determine wind direction for the initial step. It depends on the probability of each wind direction for the initial wave height
initial_direction = direction_values(index_direction); % Initial wind direction

w_forecast = zeros(365*yrs*(24/time_step),total_columns); % forecast (output) is a matrix containing time, wave and wind info for the specified number of years
w_forecast(1,[column_wave column_wind column_direction column_month column_year column_day column_hour]) = [initial_wave initial_wind initial_direction first_month first_year first_day first_hour]; % The first row of the output matrix is already known

for i=2:size(w_forecast,1) % This loop executes each step of the simulation
    
    w_forecast(i, column_hour) = w_forecast(i-1, column_hour) + time_step; % First all time-related data is updated. This just works as a calendar
    
    if(w_forecast(i, column_hour) >=24)
        w_forecast(i, column_hour) = w_forecast(i, column_hour) - 24;
        w_forecast(i, column_day) = w_forecast(i-1, column_day) + 1;
    else
        w_forecast(i, column_day) = w_forecast(i-1, column_day);
    end
    
    if(((w_forecast(i-1, column_month)==1) || (w_forecast(i-1, column_month)==3) || (w_forecast(i-1, column_month)==5) || (w_forecast(i-1, column_month)==7) || (w_forecast(i-1, column_month)==8) || (w_forecast(i-1, column_month)==10) || (w_forecast(i-1, column_month)==12)) && w_forecast(i, column_day)>31)
        w_forecast(i, column_month) = w_forecast(i-1, column_month) + 1;
        w_forecast(i, column_day) = 1; %w_forecast(i, column_day-31);
    elseif(w_forecast(i-1, column_month)==2 && w_forecast(i, column_day)>28)
        w_forecast(i, column_month) = w_forecast(i-1, column_month) + 1;
        w_forecast(i, column_day) = 1; %w_forecast(i, column_day-28);
    elseif(((w_forecast(i-1, column_month)==4) || (w_forecast(i-1, column_month)==6) || (w_forecast(i-1, column_month)==9) || (w_forecast(i-1, column_month)==11)) && w_forecast(i, column_day)>30)
        w_forecast(i, column_month) = w_forecast(i-1, column_month) + 1;
        w_forecast(i, column_day) = 1; %w_forecast(i, column_day-30);
    else
        w_forecast(i, column_month) = w_forecast(i-1, column_month);
    end
    
    if(w_forecast(i, column_month) > 12)
        w_forecast(i, column_month) = 1;
        w_forecast(i, column_year) = w_forecast(i-1, column_year) + 1;
    else
        w_forecast(i, column_year) = w_forecast(i-1, column_year); % "Calendar function" ends here
    end
    
    switch w_forecast(i, column_month) % Decides what probability matrix to use depending on the month, and the index of the wave value is generated randomly using probabilities (Markov)
        case 1
            while(Prob_Jan(index_wave,index_wave)==2) % If the probability is 2 for the wave height value, it means that said wave height does not happen in this month (and no probabilities for transitions are available), so the one immediately above (smaller wave) is selected. Unless the wave height is the first one, 0 meters (then the change is the other way)
                if(index_wave == 1) 
                    index_wave = index_wave + 1;
                    if (Prob_Jan(index_wave,index_wave)==2)
                        index_wave = index_wave + 1;
                    end
                else
                    index_wave = index_wave - 1;
                end
            end
            index_wave = random_index(Prob_Jan(index_wave,:));
        case 2
            while(Prob_Feb(index_wave,index_wave)==2) % If the probability is 2 for the wave height value, it means that said wave height does not happen in this month (and no probabilities for transitions are available), so the one immediately above (smaller wave) is selected. Unless the wave height is the first one, 0 meters (then the change is the other way)
                if(index_wave == 1) 
                    index_wave = index_wave + 1;
                    if (Prob_Feb(index_wave,index_wave)==2)
                        index_wave = index_wave + 1;
                    end
                else
                    index_wave = index_wave - 1;
                end
            end
            index_wave = random_index(Prob_Feb(index_wave,:));
        case 3
            while(Prob_Mar(index_wave,index_wave)==2) % If the probability is 2 for the wave height value, it means that said wave height does not happen in this month (and no probabilities for transitions are available), so the one immediately above (smaller wave) is selected. Unless the wave height is the first one, 0 meters (then the change is the other way)
                if(index_wave == 1) 
                    index_wave = index_wave + 1;
                    if (Prob_Mar(index_wave,index_wave)==2)
                        index_wave = index_wave + 1;
                    end
                else
                    index_wave = index_wave - 1;
                end
            end
            index_wave = random_index(Prob_Mar(index_wave,:));
        case 4
            while(Prob_Apr(index_wave,index_wave)==2) % If the probability is 2 for the wave height value, it means that said wave height does not happen in this month (and no probabilities for transitions are available), so the one immediately above (smaller wave) is selected. Unless the wave height is the first one, 0 meters (then the change is the other way)
                if(index_wave == 1) 
                    index_wave = index_wave + 1;
                    if (Prob_Apr(index_wave,index_wave)==2)
                        index_wave = index_wave + 1;
                    end
                else
                    index_wave = index_wave - 1;
                end
            end
            index_wave = random_index(Prob_Apr(index_wave,:));
        case 5
            while(Prob_May(index_wave,index_wave)==2) % If the probability is 2 for the wave height value, it means that said wave height does not happen in this month (and no probabilities for transitions are available), so the one immediately above (smaller wave) is selected. Unless the wave height is the first one, 0 meters (then the change is the other way)
                if(index_wave == 1) 
                    index_wave = index_wave + 1;
                    if (Prob_May(index_wave,index_wave)==2)
                        index_wave = index_wave + 1;
                    end
                else
                    index_wave = index_wave - 1;
                end
            end
            index_wave = random_index(Prob_May(index_wave,:));
        case 6
            while(Prob_Jun(index_wave,index_wave)==2) % If the probability is 2 for the wave height value, it means that said wave height does not happen in this month (and no probabilities for transitions are available), so the one immediately above (smaller wave) is selected. Unless the wave height is the first one, 0 meters (then the change is the other way)
                if(index_wave == 1) 
                    index_wave = index_wave + 1;
                    if (Prob_Jun(index_wave,index_wave)==2)
                        index_wave = index_wave + 1;
                    end
                else
                    index_wave = index_wave - 1;
                end
            end
            index_wave = random_index(Prob_Jun(index_wave,:));
        case 7
            while(Prob_Jul(index_wave,index_wave)==2) % If the probability is 2 for the wave height value, it means that said wave height does not happen in this month (and no probabilities for transitions are available), so the one immediately above (smaller wave) is selected. Unless the wave height is the first one, 0 meters (then the change is the other way)
                if(index_wave == 1) 
                    index_wave = index_wave + 1;
                    if (Prob_Jul(index_wave,index_wave)==2)
                        index_wave = index_wave + 1;
                    end
                else
                    index_wave = index_wave - 1;
                end
            end
            index_wave = random_index(Prob_Jul(index_wave,:));
        case 8
            while(Prob_Aug(index_wave,index_wave)==2) % If the probability is 2 for the wave height value, it means that said wave height does not happen in this month (and no probabilities for transitions are available), so the one immediately above (smaller wave) is selected. Unless the wave height is the first one, 0 meters (then the change is the other way)
                if(index_wave == 1) 
                    index_wave = index_wave + 1;
                    if (Prob_Aug(index_wave,index_wave)==2)
                        index_wave = index_wave + 1;
                    end
                else
                    index_wave = index_wave - 1;
                end
            end
            index_wave = random_index(Prob_Aug(index_wave,:));
        case 9
            while(Prob_Sep(index_wave,index_wave)==2) % If the probability is 2 for the wave height value, it means that said wave height does not happen in this month (and no probabilities for transitions are available), so the one immediately above (smaller wave) is selected. Unless the wave height is the first one, 0 meters (then the change is the other way)
                if(index_wave == 1) 
                    index_wave = index_wave + 1;
                    if (Prob_Sep(index_wave,index_wave)==2)
                        index_wave = index_wave + 1;
                    end
                else
                    index_wave = index_wave - 1;
                end
            end
            index_wave = random_index(Prob_Sep(index_wave,:));
        case 10
            while(Prob_Oct(index_wave,index_wave)==2) % If the probability is 2 for the wave height value, it means that said wave height does not happen in this month (and no probabilities for transitions are available), so the one immediately above (smaller wave) is selected. Unless the wave height is the first one, 0 meters (then the change is the other way)
                if(index_wave == 1) 
                    index_wave = index_wave + 1;
                    if (Prob_Oct(index_wave,index_wave)==2)
                        index_wave = index_wave + 1;
                    end
                else
                    index_wave = index_wave - 1;
                end
            end
            index_wave = random_index(Prob_Oct(index_wave,:));
        case 11
            while(Prob_Nov(index_wave,index_wave)==2) % If the probability is 2 for the wave height value, it means that said wave height does not happen in this month (and no probabilities for transitions are available), so the one immediately above (smaller wave) is selected. Unless the wave height is the first one, 0 meters (then the change is the other way)
                if(index_wave == 1) 
                    index_wave = index_wave + 1;
                    if (Prob_Nov(index_wave,index_wave)==2)
                        index_wave = index_wave + 1;
                    end
                else
                    index_wave = index_wave - 1;
                end
            end
            index_wave = random_index(Prob_Nov(index_wave,:));
        case 12
            while(Prob_Dec(index_wave,index_wave)==2) % If the probability is 2 for the wave height value, it means that said wave height does not happen in this month (and no probabilities for transitions are available), so the one immediately above (smaller wave) is selected. Unless the wave height is the first one, 0 meters (then the change is the other way)
                if(index_wave == 1) 
                    index_wave = index_wave + 1;
                    if (Prob_Dec(index_wave,index_wave)==2)
                        index_wave = index_wave + 1;
                    end
                else
                    index_wave = index_wave - 1;
                end
            end
            index_wave = random_index(Prob_Dec(index_wave,:));
    end
    
    w_forecast(i, column_wave) = wave_values(index_wave); % Assigns the wave height value corresponding to the index just determined
    
    index_wind = random_index(Prob_wind(index_wave,:)); % Generates random index that will determine wind speed for this step. It depends on the probability of each wind speed for the actual wave height
    w_forecast(i, column_wind) = wind_values(index_wind); % Assigns the wind speed value corresponding to the index just determined
    
    index_direction = random_index(Prob_direction(index_wave,:)); % Generates random index that will determine wind direction for this step. It depends on the probability of each wind direction for the actual wave height
    w_forecast(i, column_direction) = direction_values(index_direction); % Assigns the wind direction value corresponding to the index just determined
end

end